var logos = document.getElementsByClassName("loading__icon");

for (logo of logos) {
    console.log("found path, initializing");
    let length = init_logo_params(logo);
}

// recursive function that continually calls itself
// after a timeout for an infinite loading animation
// make sure that paths have correct style
//      i.e. css transition on stroke-dashoffset
function update_offset(path, length) {
    let timeout = 3000;
    path.style.strokeDashoffset = "0px"; // start animation
    path.getBoundingClientRect();
    // setTimeout does not block
    setTimeout(function(){ set_offset_to_path_length(path, length) }, timeout, path, length);
    setTimeout(function(){ update_offset(path, length) }, 2*timeout, path, length);
    // functions get called, but do nothing (don't trigger transition animation)
}

function set_offset_to_path_length(path, length) {
    path.style.strokeDashoffset = length;
    path.getBoundingClientRect();
}

function init_logo_params(logo) {
    let id = logo.getAttribute("id");
    // there are 4 elements. each needs transitions
    // line1
    let line1 = document.querySelector("#" + id + " > .loading__icon__line1");
    let line1_length = line1.getTotalLength();
    line1.style.strokeDasharray = line1_length + ' ' + line1_length;
    line1.style.strokeDashoffset = line1_length;
    line1.getBoundingClientRect();
    line1.style.transition = "stroke-dashoffset 500ms linear 0ms";
    update_offset(line1, line1_length);

    let curve1 = document.querySelector("#" + id + " > .loading__icon__curve1");
    let curve1_length = curve1.getTotalLength();
    curve1.style.strokeDasharray = curve1_length + ' ' + curve1_length;
    curve1.style.strokeDashoffset = curve1_length;
    curve1.getBoundingClientRect();
    curve1.style.transition = "stroke-dashoffset 500ms linear 500ms";
    update_offset(curve1, curve1_length);

    let curve2 = document.querySelector("#" + id + " > .loading__icon__curve2");
    let curve2_length = curve2.getTotalLength();
    curve2.style.strokeDasharray = curve2_length + ' ' + curve2_length;
    curve2.style.strokeDashoffset = curve2_length;
    curve2.getBoundingClientRect();
    curve2.style.transition = "stroke-dashoffset 500ms linear 1000ms";
    update_offset(curve2, curve2_length);

    let line2 = document.querySelector("#" + id + " > .loading__icon__line2");
    let line2_length = line2.getTotalLength();
    line2.style.strokeDasharray = line2_length + ' ' + line2_length;
    line2.style.strokeDashoffset = line2_length;
    line2.getBoundingClientRect();
    line2.style.transition = "stroke-dashoffset 500ms linear 1500ms";
    update_offset(line2, line2_length);
}
