function setFieldToInnerValue(valueEl, fieldId, value=null) {
    // fill in form field with correct value
    if (value) {
        document.getElementById(fieldId).setAttribute('value', value);
    } else {
        document.getElementById(fieldId).setAttribute('value', valueEl.innerText);
    }
    // set displayed selection to the correct value
    var sibling = valueEl.parentNode.firstElementChild;
    do {
        sibling.classList.remove("selected");
    } while (sibling = sibling.nextElementSibling)
    valueEl.classList.add("selected");
    document.getElementById(fieldId + '-selected').innerHTML = valueEl.innerText;
    toggleSelection(fieldId);
}
function toggleSelection(id_root) {
    var selected = document.getElementById(id_root + '-selected');
    var selection = document.getElementById(id_root + '-selection');
    if (selected && selection) {
        if (selection.classList.contains("open")) {
            selection.classList.remove("open");
            selected.classList.remove("open");
        } else {
            selection.classList.add("open");
            selected.classList.add("open");
        }
    } else {
        console.log("didn't find elements to toggle");
    }
}
function activate_form(form_id) {
    var form = document.getElementById(form_id);
    if (form) {
        form.submit();
    }
}
