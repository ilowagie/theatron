
// script needs to be loaded with attribute `endpoint` set
// this attribute needs to be set to the server-side endpoint that delivers the JSON to fill the scroll
const endpoint = document.currentScript.getAttribute("endpoint");
if (!endpoint) {
    console.log("ERROR: enpoint attribute not set");
}
const ordering = document.currentScript.getAttribute("ordering");
if (!ordering) {
    console.log("WARNING: ordering not set");
}

//first: fill up infinite_scroll:
async function fill() {
    const {scrollHeight, scrollTop, clientHeight} = document.documentElement;
    var res = true;
    fill_loop:
    while (scrollTop + clientHeight > scrollHeight - 5) {
        res = await load_more()
        if (res) {
            const {scrollHeight, scrollTop, clientHeight} = document.documentElement;
        } else {
            // false means that there are no more elements
            console.log("no more elements to use in fill");
            //stop loading animation:
            document.getElementById("movie_loading").style.display = "none";
            return res;
        }
    }
    console.log("fill finished");
}
fill().then(res => {
    if (res) {
        // res = true means that there is more to add after filling the scroll for the current scroll position
        window.addEventListener('scroll', scroll_listener);
    }
})

// add event listener for scrolling
// call load_more when user scrolls to point where there are no more movie cards
// disable when no more cards are available to load

function scroll_listener() {
    const {scrollHeight, scrollTop, clientHeight} = document.documentElement;
    if (scrollTop + clientHeight > scrollHeight - 5) {
        load_more().then(res => {
            if (!res) {
                // false means that there are no more elements
                //  `-> end reached
                scroll_end_reached();
            }
        });
    }
}

function scroll_end_reached() {
    // remove event listener
    window.removeEventListener('scroll', scroll_listener);
    // stop loading animation
    document.getElementById("movie_loading").style.display = "none";
}

// returns true if it makes sense to request more
// false if there was an unexpected error or there is no more to get
async function load_more() {
    if (typeof load_more.page == 'undefined') {
        load_more.page = 0;
    }
    load_more.page++;

    var url = endpoint + load_more.page;
    if (ordering) {
        url += "?ordering=" + ordering;
    }

    let response = await fetch(url);

    if (response.ok) {
        // handle it
        let items = await response.json();
        if (items.length > 0) {
            for (item of items) {
                // TODO: make more generic somehow
                add_movie_to_scroll(item);
            }
            return true;
        } else {
            // probably reached the end of the paging
            console.log("infinite_scroll: response was empty, so there probably are no more elements");
            return false;
        }
    } else {
        console.log("ERROR: item fetching for infinite_scroll has bad status code ("+response.status+")");
        return false;
    }
}

function add_movie_to_scroll(movie) {
    let infinite_scroll = document.getElementById("infinite-scroll");

    if (!infinite_scroll) {
        console.log("ERROR: could not find infinite_scroll element to add elements to");
        return;
    }

    // should result in exactly the same HTML as in
    //  `templates/macros/movie_macros.html.tera`
    let card = document.createElement("A");
    card.setAttribute("href", ("/movies/view/" + movie.id));
    card.setAttribute("class", "movie_card");

        let image = document.createElement("IMG");
        image.setAttribute("src", movie.poster_location);
        image.setAttribute("class", "movie_card__poster");
        card.appendChild(image);

        let card_info = document.createElement("DIV");
        card_info.setAttribute("class", "movie_card__info");

            let card_info_title = document.createElement("DIV");
            card_info_title.setAttribute("class", "movie_card__info__title");
            card_info_title.innerHTML  = "<b>" + movie.title + "</b>";
            card_info.appendChild(card_info_title);

            let card_info_duration = document.createElement("DIV");
            card_info_duration.setAttribute("class", "movie_card__info__duration");
            card_info_duration.innerHTML = "<b>" + movie.mins_duration + "mins</b>";
            card_info.appendChild(card_info_duration);

        card.appendChild(card_info);

    infinite_scroll.appendChild(card);
}
