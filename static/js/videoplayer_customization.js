// Loosly based on the customizations from
//      https://developer.mozilla.org/en-US/docs/Web/Guide/Audio_and_video_delivery/cross_browser_video_player
//  and https://developer.mozilla.org/en-US/docs/Web/Guide/Audio_and_video_delivery/Video_player_styling_basics


// initialization
// ==============

var videoContainer = document.getElementById("videoplayer-container");
var video = document.getElementById("videoplayer");
var videoControls = document.getElementById("videoplayer-controls");

video.controls = false; // in js so that if js is off, default controls are visible
videoControls.dataset.state = "visible";

var subtitles = document.getElementById("subtitle-select");
var selected_sub;

if (subtitles) {
    for (var i = 0; i < video.textTracks.length; i++) {
        video.textTracks[i].mode = 'hidden';
        let text_track_id = video.textTracks[i].id;
        let sub_select = document.getElementById('select-' + text_track_id);
        if (sub_select) {
            sub_select.addEventListener('click', function(e) {
                if (selected_sub) {
                    document.getElementById('select-' + selected_sub.id).dataset.state = "notselected";
                    selected_sub.mode = 'hidden';
                } else {
                    document.getElementById('select-sub-none').dataset.state = "notselected";
                }

                this.dataset.state = 'selected';
                let desired_id = this.id.substr(7, this.id.length);

                // the video's text track is not same as DOM element, but has same id???
                search_loop: for (var i = 0; i < video.textTracks.length; i++) {
                    if (video.textTracks[i].id == desired_id) {
                        selected_sub = video.textTracks[i];
                        break search_loop;
                    }
                }

                selected_sub.mode = 'showing';
            });
        } else {
            console.log("subtitle track " + i + " has no select in subtitle menu");
        }
    }
    // attach click handler to disable subtitles
    document.getElementById('select-sub-none').addEventListener('click', function(e) {
        if (selected_sub) {
            document.getElementById('select-' + selected_sub.id).dataset.state = "notselected";
            selected_sub.mode = 'hidden';
        }

        this.dataset.state = 'selected';

        selected_sub = null;
    });
}

// Control "hide-and-seek"
// =======================


var timeout_hide = null;
function timeout_hide_function(videoControls) {
    if (document.getElementById("playpause").dataset.state === "pause") {
        videoControls.dataset.state = "hidden";
    }
}
function remove_timeout_hide() {
    if (timeout_hide !== null) {
        clearTimeout(timeout_hide);
    }
    timeout_hide = null;
}

video.addEventListener('pointermove', (event) => {
    videoControls.dataset.state = "visible";
    if (timeout_hide === null) {
        timeout_hide = setTimeout(function() { timeout_hide_function(videoControls) }, 2000, videoControls);
    } else {
        // reset timer
        clearTimeout(timeout_hide);
        timeout_hide = setTimeout(function() { timeout_hide_function(videoControls) }, 2000, videoControls);
    }
}, videoControls);


// toggling icons
// ==============


function set_icon_go_fullscreen() {
    document.getElementById('icon-exit-fullscreen').style.display = "none";
    document.getElementById('icon-go-fullscreen').style.display = "";
}
function set_icon_exit_fullscreen() {
    document.getElementById('icon-go-fullscreen').style.display = "none";
    document.getElementById('icon-exit-fullscreen').style.display = "";
}
function set_icon_play() {
    document.getElementById('icon-pause').style.display = "none";
    document.getElementById('icon-play').style.display = "";
}
function set_icon_pause() {
    document.getElementById('icon-play').style.display = "none";
    document.getElementById('icon-pause').style.display = "";
}
function set_icon_mute() {
    document.getElementById('icon-mute').setAttribute('stroke-width', '0');
}
function set_icon_unmute() {
    document.getElementById('icon-mute').setAttribute('stroke-width', '2');
}


// progress
// ========


var progress = document.getElementById("video-progress");
video.addEventListener('loadedmetadata', function() {
    progress.setAttribute('max', video.duration);
});
video.addEventListener('timeupdate', function() {
    progress.setAttribute('value', video.currentTime);
});
// skip ahead
progress.addEventListener('click', function(e) {
    var position = (e.pageX - this.offsetLeft) / this.offsetWidth;
    video.currentTime = position * video.duration;
});


// fullscreen API wrappers
// =======================


var fullscreenEnabled = !!(document.fullscreenEnabled
                           || document.mozFullScreenEnabled
                           || document.webkitSupportsFullscreen
                           || document.webkitFullscreenEnabled
                           || document.createElement('video').webkitRequestFullScreen);

function isFullscreen() {
    return !!(document.fullscreen
              || document.webkitIsFullScreen
              || document.mozFullScreen
              || document.fullscreenElement);
}

function setFullscreenData(state) {
    document.getElementById('fullscreen').setAttribute('data-state', 'exit-fullscreen');
    videoContainer.setAttribute('data-fullscreen', !!state);
    if (!!state) {
        set_icon_exit_fullscreen();
    } else {
        set_icon_go_fullscreen();
    }
}

function handleFullscreen() {
    if (isFullscreen()) {
        if (document.exitFullscreen) document.exitFullscreen();
        else if (document.mozCancelFullScreen) document.mozCancelFullScreen();
        else if (document.webkitCancelFullScreen) document.webkitCancelFullScreen();
        setFullscreenData(false);
    } else {
        if (videoContainer.requestFullscreen) videoContainer.requestFullscreen();
        else if (videoContainer.mozRequestFullScreen) videoContainer.mozRequestFullScreen();
        else if (videoContainer.webkitRequestFullScreen) videoContainer.webkitRequestFullScreen();
        setFullscreenData(true);
    }
}


// buttons
// =======

//  ->  Play/Pause
//      ----------

function togglePlay(video, button) {
    if (video.paused || video.ended) {
        button.dataset.state = 'pause';
        set_icon_pause();
        video.play();
        timeout_hide = setTimeout(function() { timeout_hide_function(videoControls) }, 1000, button.parentNode);
    } else {
        button.dataset.state = 'play';
        set_icon_play();
        remove_timeout_hide();
        button.parentNode.dataset.state = "visible";
        video.pause();
    }
}
var playpause = document.getElementById("playpause");
playpause.addEventListener('click', function(e) {
    togglePlay(video, this);
});

// take right-click actions into account:
video.addEventListener('play', function() {
    let button = document.getElementById('playpause');
    button.dataset.state = 'pause';
    timeout_hide = setTimeout(function() { timeout_hide_function(videoControls) }, 1000, document.getElementById("videoplayer-controls"));
});
video.addEventListener('pause', function() {
    remove_timeout_hide();
    let button = document.getElementById('playpause');
    button.dataset.state = 'play';
});

//  ->  Mute/Unmute
//      -----------

var mute = document.getElementById("mute");
mute.addEventListener('click', function(e) {
    this.dataset.state = video.muted ? 'mute' : 'unmute';
    if (this.dataset.state === 'mute') {
        set_icon_mute();
    } else {
        set_icon_unmute();
    }
    video.muted = !video.muted;
});
// take right-click actions into account
video.addEventListener('mute', function() {
    document.getElementById("mute").dataset.state = "unmute";
    set_icon_unmute();
});
video.addEventListener('unmute', function() {
    document.getElementById("mute").dataset.state = "mute";
    set_icon_mute();
});

//  ->  Fullscreen
//      ----------

var fullscreen = document.getElementById("fullscreen");
if (!fullscreenEnabled) {
    fullscreen.style.display = 'none';
} else {
    fullscreen.addEventListener('click', function(e) {
        handleFullscreen();
    });
}

//  ->  Subtitles
//      ---------

function toggle_sub_select_menu_visibility() {
    let select_menu = document.getElementById('sub-select-menu');
    select_menu.dataset.state = (select_menu.dataset.state == 'hidden') ? 'visible' : 'hidden';
}

if (subtitles) {
    subtitles.addEventListener('click', function(e) {
        toggle_sub_select_menu_visibility();
    });
}


// Hotkey implementations
// ======================


document.addEventListener("keydown", handle_key);

function handle_key(event) {
    // to counteract firefox bug 354358?
    if (event.isComposing || event.keyCode === 229) {
        return;
    }
    // normal key handling:
    switch (event.keyCode) {
        case 70: // F
            handleFullscreen();
            break;
        case 80: // P
        case 32: // space
            let button = document.getElementById("playpause");
            togglePlay(video, button);
            break;
    }
}


// Size adjustments
// ================


function handle_resize() {
    video.width = window.innerWidth;
    videoContainer.style.height = "";
    videoContainer.style.width = window.innerWidth + 'px';
    if (video.clientHeight > window.innerHeight) {
        video.height = window.innerHeight;
        videoContainer.style.width = "";
        videoContainer.style.height = window.innerHeight + 'px';
    }
}

video.onloadedmetadata = function () {
    if (this.clientHeight + 20 > window.innerHeight) {
        let new_height = window.innerHeight - 20;
        videoContainer.style.width = "";
        videoContainer.style.height = new_height + 'px';
        video.height = new_height;
    }
}

// handle resize on script load to start with correct sizes
handle_resize();
// add event listener
window.addEventListener('resize', handle_resize);

