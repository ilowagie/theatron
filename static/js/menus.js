function toggleMenu(id) {
    // content block
    let divElem = document.getElementById(id);
    // arrow next to block title
    let iElem = document.getElementById(id.concat("-arrow"));
    if (divElem && iElem) {
        if (divElem.classList.contains("menu_show")) {
            // content goes expanded -> collapsed
            // arrow goes down -> left
            divElem.classList.remove("menu_show");
            iElem.classList.remove("down");
            iElem.classList.add("left");
            divElem.style.overflow = "hidden";
        } else {
            // content goes collapsed -> expanded
            // arrow goes left -> down
            divElem.classList.add("menu_show");
            iElem.classList.remove("left");
            iElem.classList.add("down");
            setTimeout(function(){document.getElementById(id).style.overflow = "visible"}, 300)
        }
    } else {
        console.log("didn't find menu elements to toggle");
    }
}
