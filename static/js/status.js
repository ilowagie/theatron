draw_status();

async function draw_status() {
    // There are multiple ways this function can go wrong.
    // fetch can return 404, json data can be something unexpected, status is not recognized...
    // When things go wrong, just ignore it or, at most, report it
    let get_success = await get_status("/admin/scrub_status_json");
    while (get_success) {
        // once we have something to draw, start a loop
        // scrub_status_update blocks untill there's an update
        get_success = await get_status("/admin/scrub_status_update");

    }
}

// returns true on success, false otherwise
async function get_status(endpoint) {
    let response = await fetch(endpoint);
    if (response.status == 200) {
        // handle it
        let status_struct = await response.json();
        switch (status_struct.status) {
            case "inactive":
                draw_scrubber_inactive();
                break;
            case "working":
                draw_scrubber_busy(status_struct);
                break;
            case "done":
                draw_scrubber_done(status_struct);
                await setTimeout(() => {}, 60000); // give some time to read results
                break;
            default:
                console.log("unexpected status received: " + status_struct.status);
        }
        return true;
    } else if (response.status == 204) {
        return true;
        // no content
        //  `-> when the wait on an update has timed out, server will respond with 204: no content
        //      if we're still on the status page: just resend request
    } else {
        console.log("error: status response has bad status code ("+response.status+")");
        return false;
    }
}

// dynamic drawing functions

    // helpers
function update_error_list(errors) {
    var error_list = document.getElementById("scrub-error-list");
    // empty old values from list
    while (error_list.firstChild) {
        error_list.removeChild(error_list.firstChild);
    }
    // fill list with new values
    for (error of errors) {
        var li_node = document.createElement("LI");
        li_node.innerHTML = error;
        error_list.appendChild(li_node);
    }
}
function update_progress(progress_id_root, value, max) {
    var progress_el = document.getElementById(progress_id_root + "-progress");
    progress_el.setAttribute("value", value);
    progress_el.setAttribute("max", max);
    var progress_text = document.getElementById(progress_id_root + "-progress-txt");
    progress_text.innerHTML = value + ' / ' + max;
}

    // call when scrubber is inactive
function draw_scrubber_inactive() {
    document.getElementById("scrub-status-value").innerHTML = "Inactive";
    document.getElementById("scrub-progress-row").style.display = "none";
    document.getElementById("transcoding-progress-row").style.display = "none";
    document.getElementById("scrub-error-block").style.display = "none";
}
    // call when scrubber is busy, include progress
function draw_scrubber_busy(status_struct) {
    document.getElementById("scrub-status-value").innerHTML = "Working";
    document.getElementById("scrub-progress-row").style.display = "";
    update_progress("scrub", status_struct.done_amount, status_struct.entry_amount);
    if (status_struct.transcoding_amount > 0) {
        document.getElementById("transcoding-progress-row").style.display = "";
        update_progress("transcoding", status_struct.transcoding_done, status_struct.transcoding_amount);
    } else {
        document.getElementById("transcoding-progress-row").style.display = "none";
    }
    document.getElementById("scrub-error-block").style.display = "";
    var error_header = document.getElementById("scrub-errors-header");
    error_header.innerHTML = status_struct.failed_amount + ' errors';
    update_error_list(status_struct.errors);
}
    // call when scrubber is done
function draw_scrubber_done(status_struct) {
    document.getElementById("scrub-status-value").innerHTML = "Done";
    document.getElementById("scrub-progress-row").style.display = "none";
    document.getElementById("transcoding-progress-row").style.display = "none";
    document.getElementById("scrub-error-block").style.display = "";
    var error_header = document.getElementById("scrub-errors-header");
    error_header.innerHTML = status_struct.failed_amount + ' errors';
    update_error_list(status_struct.errors);
}
