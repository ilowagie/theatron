use rocket::http;
use rocket::http::hyper::header::Range as hyperRange;
use rocket::http::hyper::header as hyperHeader;
use rocket::response::{self, Response, Responder};
use rocket::Outcome;
use rocket::request::{self, Request, FromRequest};
use std::str::FromStr;
use std::fs::File;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::io::SeekFrom;
use std::path::PathBuf;
use std::os::unix::io::{AsRawFd, RawFd};

// 5 MiB will most likely make it so that all moov metadata from faststart mp4 files is inlcuded in
//   a single http response, the first one
const PARTIAL_CONTENT_MAX_SIZE: u64 = 5242880; // 5 MiB

pub struct PartialContent {
    range: hyperRange
}
pub struct ChunkedFileInfo {
    size: u64,
    content_type: http::ContentType,
    file_location: String
}

pub struct MovieStreamChunk {
    pub has_error: bool,
    range_start: u64,
    file_info: Option<ChunkedFileInfo>,
}

// stream part of big file
//      TODO: set size per http packet
//      Stream packet size in chunks of DEFAULT_CHUNK_SIZE
impl<'r> Responder<'r> for MovieStreamChunk {
    fn respond_to(self, request: &Request) -> response::Result<'r> {
        if self.has_error {
            response::status::NotFound("experienced errors while getting movie part").respond_to(request)
        } else {
            let file_info_unwrapped = self.file_info.as_ref().expect("no file info in MovieStreamChunk with no errors");
            let read_size = self.get_read_size();
            let read_end = read_size + self.range_start - 1;

            let file = File::open(&file_info_unwrapped.file_location).unwrap();
            let mut reader = BufReader::new(file);
            reader.seek(SeekFrom::Start(self.range_start));

            // respond as in https://tools.ietf.org/html/rfc7233#section-4.1
            Response::build()
                .status(http::Status::PartialContent)
                .raw_header("Content-Range", format!("bytes {}-{}/{}", self.range_start,
                                                                       read_end,
                                                                       file_info_unwrapped.size))
                .raw_header("Content-Length", format!("{}", read_size))
                .header(file_info_unwrapped.content_type.clone())
                .streamed_body(reader.take(PARTIAL_CONTENT_MAX_SIZE))
                .ok()
        }
    }
}
impl MovieStreamChunk {
    pub fn fill_in_file_info(&mut self, path: &PathBuf) -> io::Result<()> {
        let file = File::open(path)?;
        let met = file.metadata()?;
        let file_length = met.len();
        let mime_string = crate::get_mime_type_string(path.to_str().unwrap())
                            .map_err(|e| io::Error::new(io::ErrorKind::Other, e))?;
        let splitted_mime_string: Vec<String> = mime_string.split('/').map(|val| String::from(val)).collect();
        if splitted_mime_string.len() != 2 {
            return Err(io::Error::new(io::ErrorKind::Other, "Malformed mime type string"))
        }
        let content_type = http::ContentType::new(splitted_mime_string[0].clone(), splitted_mime_string[1].clone());

        self.file_info = Some(ChunkedFileInfo {
            size: file_length,
            content_type: content_type,
            file_location: path.to_string_lossy().to_owned().to_string()
        });

        Ok(())
    }
    fn get_read_size(&self) -> u64 {
        let unlimited_size = if let Some(ref f_info_uw) = self.file_info {
            f_info_uw.size - self.range_start + 1
        } else {
            panic!("get read size issued on empty file info")
        };
        if unlimited_size < PARTIAL_CONTENT_MAX_SIZE {
            unlimited_size
        } else {
            PARTIAL_CONTENT_MAX_SIZE
        }
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for PartialContent {
    type Error = String;

    fn from_request(request: &'a Request<'r>) -> request::Outcome<Self, Self::Error> {
        if let Some(range) = request.headers().get_one("range") {
            let part = PartialContent {
                range: hyperRange::from_str(range).unwrap()
            };
            Outcome::Success(part)
        } else {
            Outcome::Failure((http::Status::BadRequest, String::from("no range found")))
        }
    }
}

fn extract_start_from_range(range: &hyperRange) -> Option<u64> {
    if let hyperRange::Bytes(ref v) = range {
        match v[0] {
            hyperHeader::ByteRangeSpec::FromTo(from, _) => Some(from),
            hyperHeader::ByteRangeSpec::AllFrom(from) => Some(from),
            hyperHeader::ByteRangeSpec::Last(end) => return None
        }
    } else {
        None
    }

}

impl From<PartialContent> for MovieStreamChunk {
    fn from(part: PartialContent) -> Self {
        let range_start = extract_start_from_range(&part.range)
                            .expect("could not extract range start");

        MovieStreamChunk {
            has_error: false,
            range_start: range_start,
            file_info: None
        }
    }
}


