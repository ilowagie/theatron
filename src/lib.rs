#![feature(proc_macro_hygiene, decl_macro)]

#![allow(unused_imports)]
#![allow(unused_variables)]
#![allow(non_camel_case_types)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate lazy_static;
#[macro_use] mod general_util_macros;

extern crate time;
extern crate chrono;

use time::Duration;
use std::collections::HashMap;
use std::path::Path;
use std::fs;
use std::fs::File;
use std::io::Result as ioResult;
use std::io::Error as ioError;
use std::io::ErrorKind as ioErrorKind;
use std::io::BufReader;
use std::io::prelude::*;
use std::sync::RwLock;
use std::process::Command;
use rocket::http::{Cookie, Cookies, SameSite};
use rocket::response::Redirect;
use rocket_contrib::templates::Template;
use rocket_contrib::serve::StaticFiles;
use serde::Deserialize;

mod rfc7233;
mod admin;
mod movies;
mod sass_fairing;

mod scrubber;
mod datahandling;

pub struct Logger {
    time_format: String,
    log_file:    fs::File,
}
impl Logger {
    fn log(&mut self, msg: &str) {
        let t = chrono::Local::now().format(self.time_format.as_str());
        if let Err(e) = writeln!(self.log_file, "{} | {}", t, msg) {
            eprintln!("Couldn't write to log file: {}", e);
        }
    }
}
pub struct RocketConfig {
    env_is_dev:     bool,
    db_path:        String,
}
#[derive(Deserialize)]
pub struct UserConfig {
    pub log_time_format: String,
    pub accessible: Vec<String>,
    pub video_dest: String,
    pub image_dir: String,
    pub subtitle_dir: String,
    pub update_timeout_secs: u64,
    pub title_word_delim: String,
    pub paging: i32,
    pub limit_requests: bool,
    pub subtitle_langs: Vec<String>,
    pub allowed_video_formats: HashMap<String, Vec<String>>,
    pub ffmpeg_enable_vaapi: bool,
    pub api_order: Vec<String>,
    pub apis: Option<HashMap<String, String>> // will be filled in when reading .secrets
}
impl UserConfig {
    fn default_avf() -> HashMap<String, Vec<String>> {
        let mut res = HashMap::new();
        res.insert(String::from("mp4"), vec![String::from("h264")]);
        res
    }
    pub fn default() -> Self {
        UserConfig {
            log_time_format: String::from("%Y-%m-%d %H:%M:%S"),
            accessible: Vec::new(),
            video_dest: String::from("./videos"),
            image_dir: String::from("./images"),
            subtitle_dir: String::from("./subtitles"),
            update_timeout_secs: u64::MAX,
            title_word_delim: String::from("_"),
            paging: 10,
            limit_requests: false,
            subtitle_langs: Vec::new(),
            allowed_video_formats: UserConfig::default_avf(),
            ffmpeg_enable_vaapi: false,
            api_order: Vec::new(),
            apis: None
        }
    }
    pub fn parse_config(file_name: &String) -> Result<Self, String> {
        let file = match File::open(file_name) {
            Err(e) => return Err(format!("{:?}", e)),
            Ok(val) => val
        };
        let reader = BufReader::new(file);
        let val = match serde_json::from_reader(reader) {
            Err(e) => Err(format!("{:?}", e)),
            Ok(val) => Ok(val)
        }?;
        Ok(val)
    }
    pub fn parse_config_fallback() -> Self {
        let location = String::from("config/theatron.json");
        match Self::parse_config(&location) {
            Ok(val) => val.fill_in_api_keys().unwrap_or(Self::default()),
            Err(_) => Self::default()
        }
    }
    fn fill_in_api_keys(mut self) -> Result<Self, String> {
        let mut api_map = read_api_key().map_err(|e| format!("received error while reading keys: {}", e))?;

        // check if all apis from api_order are present:
        for i in 0..self.api_order.len() {
            if api_map.get(&self.api_order[i]).is_none() {
                self.api_order.remove(i);
            } else if datahandling::api_abstractions::Api::from(&self.api_order[i])
                        == datahandling::api_abstractions::Api::Unknown {
                return Err(format!("could not parse '{}' as an api", self.api_order[i]))
            }
        }

        self.apis = Some(api_map);

        Ok(self)
    }
    pub fn get_api_key(&self, api_name: &String) -> Result<String, String> {
        let default_map = HashMap::new();
        let map = self.apis.as_ref().unwrap_or(&default_map);
        if let Some(key) = map.get(api_name) {
            Ok(key.clone())
        } else {
            // this should not be possible, since it was checked when initializing config
            // but don't panic just to be sure
            Err(format!("could not find key for {}", api_name))
        }
    }
}

pub fn get_mime_type_string(file: &str) -> Result<String, String> {
    let file_cmd = Command::new("file").arg("-b")
                                       .arg("--mime-type")
                                       .arg(file)
                                       .output();
    if let Ok(out) = file_cmd {
        let stdout = String::from_utf8_lossy(&out.stdout);
        if out.status.success() {
            Ok(stdout.to_owned().to_string().trim().to_string())
        } else {
            Err(format!("errors encountered while getting movie_location mime_type {}", String::from_utf8_lossy(&out.stderr)))
        }
    } else {
        Err(format!("errors encountered while trying to find movie mime type"))
    }
}

#[get("/")]
fn index(cookies: Cookies) -> Redirect {
    let url_string = match cookies.get("lastTab") {
        None => String::from("/movies"),
        Some(cookie) => format!("/{}", cookie.value()),
    };
    Redirect::to(url_string)
}

#[get("/series")]
fn series_index(mut cookies: Cookies) -> Template {
    crate::add_pref_cookie!("lastTab", "series", cookies);
    let ctx = HashMap::<&str, &str>::new();
    Template::render("series/series_index", &ctx)
}

fn read_api_key() -> ioResult<HashMap<String, String>> {
    let file = File::open("config/.secrets.json")?;
    let reader = BufReader::new(file);
    let json: serde_json::Value = serde_json::from_reader(reader)?;

    if let Some(value) = json.get("api_key_map") {
        let map: HashMap<String, String> = serde_json::from_value(value.clone()).map_err(|e| std::io::Error::new(std::io::ErrorKind::Other,
                                                                                             format!("unable to parse json: {}", e)))?;
        Ok(map)
    } else {
        Err(ioError::new(ioErrorKind::NotFound, "couldn't find imdb api token in .secrets.json file"))
    }
}

pub fn rocket_init() -> rocket::Rocket {
    static REQUIRED_DB_VERSION :i32 = 3;
    static SASS_DIR :&'static str = "./sass";
    static CSS_DIR  :&'static str = "./static/css"; // static static variables are static
    static MIGRATIONS_DIR :&'static str = "./migrations";

    let config_path = String::from("config/theatron.json");

    let mut instance = rocket::ignite();
    let cfg = instance.config();
    let db_path = cfg.extras.get("databases").unwrap()
                            .get("theatron_db").unwrap()
                            .get("url").unwrap()
                            .as_str().unwrap();

    datahandling::common::startup_db(db_path, REQUIRED_DB_VERSION, MIGRATIONS_DIR);

    let log_path = cfg.extras.get("logfile").unwrap().as_str().unwrap();

    let mut using_default = false;
    let mut user_config_error = String::new();
    let userConfig = match UserConfig::parse_config(&config_path) {
        Ok(val) => val,
        Err(e) => {
            user_config_error = e;
            using_default = true;
            UserConfig::default()
        }
    };
    let rocketConfig = RocketConfig {
                        env_is_dev: cfg.environment.is_dev(),
                        db_path: String::from(db_path)
                     };
    let mut logger = Logger {
        time_format: userConfig.log_time_format,
        log_file:    fs::OpenOptions::new().write(true)
                                           .create(true)
                                           .append(true)
                                           .open(log_path)
                                           .unwrap()
    };
    if rocketConfig.env_is_dev {
        logger.log("log file initialized");
    }
    if using_default {
        logger.log(user_config_error.as_str());
        logger.log(format!("user config had following errors: {}", user_config_error).as_str());
        logger.log("using default user config values as fallback");
    }

    let dirs = admin::datadirs::new(userConfig.accessible, userConfig.image_dir.as_str(), userConfig.video_dest.as_str());
    if rocketConfig.env_is_dev {
        logger.log(format!("found accessible dirs to be {:?}", dirs.accessible).as_str());
    }

    if rocketConfig.env_is_dev {
        instance = instance.attach(sass_fairing::build_sass(SASS_DIR, CSS_DIR));
    }
    instance = instance.mount("/", routes![index, movies::movie_index, series_index, admin::admin_index])
                       .mount("/admin/", routes![admin::status, admin::db_mgmt, admin::scrub_request,
                                                 admin::scrub_status, admin::scrub_status_update])
                       .mount("/movies", routes![movies::view_movie, movies::stream, movies::more_movies])
                       .mount("/", StaticFiles::from("static/"))
                       .manage(dirs)
                       .manage(RwLock::new(scrubber::common::Scrubber::new()))
                       .manage(RwLock::new(rocketConfig))
                       .manage(RwLock::new(logger))
                       .attach(datahandling::common::TheatronDB::fairing())
                       .attach(Template::fairing());

    instance
}
