use time::Duration;
use rocket_contrib::templates::Template;
use rocket_contrib::json::Json;
use serde::{Serialize, Deserialize};
use rocket::http::{self, Cookie, Cookies, SameSite};
use rocket::http::hyper::header::Range as hyperRange;
use rocket::http::hyper::header as hyperHeader;
use rocket::response::{self, Response, Responder};
use rocket::Outcome;
use rocket::request::{self, Request, FromRequest};
use tera::Context;
use std::path::{Path, PathBuf};
use std::process::Command;
use std::io::BufReader;
use std::fs::File;
use std::io::{Cursor, SeekFrom, Seek, Read};

use crate::datahandling;
use crate::datahandling::movies as movieHandling;
use crate::UserConfig;
use crate::datahandling::common::DataOrder;
use crate::datahandling::common::TheatronDB;
use crate::rfc7233;

#[get("/movies?<ordering>")]
pub fn movie_index(ordering: Option<String>, conn: TheatronDB, mut cookies: Cookies) -> Template {
    crate::add_pref_cookie!("lastTab", "movies", cookies);

    let order_val = if let Some(order) = ordering {
        crate::add_pref_cookie!("movieOrdering", order.clone(), cookies);
        DataOrder::from_str(order.clone().as_str()).unwrap_or(DataOrder::recenttoold)
    } else {
        match cookies.get("movieOrdering") {
            None => DataOrder::recenttoold,
            Some(cookie) => DataOrder::from_str(cookie.value()).unwrap_or(DataOrder::recenttoold)
        }
    };

    let user_config = UserConfig::parse_config_fallback();
    let first_batch = movieHandling::query_movies_lazy(&*conn, &order_val, 0, user_config.paging);

    // take all ordering values and the position of the current ordering.
    //  used to display dynamic ordering menu on page
    let all_order_strings = DataOrder::all_stringified_movie_values();
    let cur_ordering_pos = all_order_strings.iter().position(|order| *order==order_val.as_display_string()).unwrap();

    let mut ctx = Context::new();
    ctx.insert("possible_orderings", &all_order_strings);
    ctx.insert("order_i", &cur_ordering_pos);
    ctx.insert("movies", &first_batch);

    Template::render("movies/movie_index", &ctx)
}

// returns JSON with page of movies.
// The caller can assume that if the currently requested page is empty, the next one will be too
#[get("/page/<page_n>?<ordering>")]
pub fn more_movies(page_n: i32, ordering: Option<String>, conn: TheatronDB, cookies: Cookies) -> Json<Vec<movieHandling::Movie>> {
    let order_val = if let Some(order) = ordering {
        DataOrder::from_str(order.clone().as_str()).unwrap_or(DataOrder::recenttoold)
    } else {
        match cookies.get("movieOrdering") {
            None => DataOrder::recenttoold,
            Some(cookie) => DataOrder::from_str(cookie.value()).unwrap_or(DataOrder::recenttoold)
        }
    };

    let user_config = UserConfig::parse_config_fallback();
    let batch = movieHandling::query_movies_lazy(&*conn, &order_val, page_n, user_config.paging);

    Json(batch)
}

#[get("/view/<id>")]
pub fn view_movie(id: i32, conn: TheatronDB) -> Result<Template, response::status::NotFound<String>> {
    let movie = movieHandling::get_movie_from_local_id(&*conn, id)
                .map_err(|e| response::status::NotFound(e))?;
    let movie_subs = movieHandling::find_all_subtitles_for_movie(&*conn, movie.id);
    // check video existance and get mime_type
    let mime_type = crate::get_mime_type_string(movie.movie_location.as_str()).map_err(|e| response::status::NotFound(e))?;

    let mut ctx = Context::new();
    ctx.insert("movie", &movie);
    ctx.insert("mime_type", &mime_type);
    ctx.insert("movie_src", &movie.movie_location.trim_start_matches("static/"));
    ctx.insert("movie_subs", &movie_subs);
    Ok(Template::render("movies/view", &ctx))
}

#[get("/stream/<movie_file..>")]
pub fn stream(movie_file: PathBuf, part: rfc7233::PartialContent) -> rfc7233::MovieStreamChunk {
    let mut responder = rfc7233::MovieStreamChunk::from(part);

    if let Err(e) = responder.fill_in_file_info(&movie_file) {
        println!("{:?}", e);
        responder.has_error = true;
    }

    responder
}
