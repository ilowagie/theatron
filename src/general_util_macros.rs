#[macro_export]
macro_rules! add_pref_cookie {
    ($name:expr, $value:expr, $cookies:ident) => {
        $cookies.add(Cookie::build($name, $value).max_age(Duration::weeks(50)).same_site(SameSite::Strict).finish())
    }
}

#[macro_export]
macro_rules! simple_error_handling {
    ($result:expr, $msg:expr) => {
        match $result {
            Ok(val) => val,
            Err(_) => return Err($msg)
        }
    }
}

