/* Sass fairing:                                *
 *      allows updating scss without rebuilding */

use std::io;
use std::path::Path;
use std::collections::HashMap;
use std::process::Command;
use std::fs;
use std::time;
use std::ffi::OsString;
use std::cell::RefCell;
use std::sync::Mutex;

use rocket::{Request, Data};
use rocket::fairing::{Fairing, Info, Kind};
use rocket::http::Method;

pub struct Sass<'a> {
    sass_dir_path : &'a Path,
    css_dest_path : &'a Path,
    file_data_mut : Mutex<RefCell<HashMap::<OsString, time::SystemTime>>>,
}

pub fn build_sass(_sass_dir_path: &'static str, _css_dest_path: &'static str) -> Sass<'static> {
    let result = Sass {
        sass_dir_path: Path::new(_sass_dir_path),
        css_dest_path: Path::new(_css_dest_path),
        file_data_mut: Mutex::new(RefCell::new(HashMap::new()))
    };
    if let Err(e) = result.update() {
        println!("update of css resulted in error {}", e);
    }
    result
}

impl Sass<'_> {
    fn update(&self) -> io::Result<()> {
        let file_data_mut_lock = self.file_data_mut.lock().unwrap();
        let mut file_data = file_data_mut_lock.borrow_mut();
        if self.sass_dir_path.is_dir() {
            let mut src: Option<OsString>;
            let mut dst: Option<&str>;
            for entry_p in fs::read_dir(self.sass_dir_path)?.filter(|entry_res|
                match entry_res {
                    Err(_) => false,
                    Ok(entry) => match entry.file_type() {
                        Err(_) => false,
                        Ok(ft) => ft.is_file()
                    }
                })
            {
                let entry = entry_p?;
                let path = entry.path().into_os_string();
                let old_systime = match file_data.get(&path) {
                    None => time::SystemTime::UNIX_EPOCH, // The oldest of old, always out of date normally
                    Some(&t) => t
                };
                let modified = entry.metadata()?.modified()?;
                if modified > old_systime {
                    // scss file was recently updated and needs to be recompiled
                    src = Some(path.clone());
                    let fstem = Path::new(&path).file_stem();
                    if fstem == None {
                        continue;
                    }
                    let mut tmp = self.css_dest_path.join(fstem.unwrap());
                    tmp.set_extension("css");
                    dst = tmp.as_os_str().to_str();
                    Command::new("sassc").arg(src.expect("src empty"))
                                         .arg(dst.expect("dst empty"))
                                         .status()?;
                    file_data.insert(path.as_os_str().to_os_string(), modified);
                }
            }
        } else {
            return Err(io::Error::new(io::ErrorKind::Other, "given sass directory entry is not a directory"));
        }
        Ok(())
    }
}

impl Fairing for Sass<'static> {
    // at least I could keep these simple
    fn info(&self) -> Info {
        Info {
            name: "Sass fairing",
            kind: Kind::Request
        }
    }

    fn on_request(&self, request: &mut Request, _: &Data) {
        if request.method() == Method::Get {
            if let Err(e) = self.update() {
                println!("update of css resulted in error {}", e);
            }
        }
    }
}
