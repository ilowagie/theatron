use super::common::*;
use super::movies::*;
use super::series::*;

use std::collections::HashMap;
use rocket_contrib::json::JsonValue;
use serde_json::value::Value as serdeValue;
use reqwest::Client as RqClient;
use serde::{Serialize, Deserialize};

use crate::UserConfig;

// TODO: implement (or derive if possible) Display
#[derive(Serialize, Deserialize, Hash, PartialEq, Eq)]
pub enum Api {
    Unknown,
    ImdbApi
}
pub fn convert_to_api_enum(api_string: &String) -> Api {
    match api_string.as_str() {
        "imdb-api" => Api::ImdbApi,
        _ => Api::Unknown
    }
}
impl From<String> for Api {
    fn from(_string: String) -> Self {
        match _string.as_str() {
            "imdb-api" => Api::ImdbApi,
            _ => Api::Unknown
        }
    }
}
impl From<&String> for Api {
    fn from(_string: &String) -> Self {
        match _string.as_str() {
            "imdb-api" => Api::ImdbApi,
            _ => Api::Unknown
        }
    }
}

async fn fill_in_person__imdb_api(mut person: Person, response: &JsonValue, cfg: &UserConfig) -> Result<Person, String> {

    person.imdb_id = crate::extract_value_from_json_string!(response, "id").clone();

    let mut dir_new = cfg.image_dir.clone();
    if !dir_new.as_str().ends_with('/') {
        dir_new.push_str("/");
    }
    dir_new.push_str("picture_");
    dir_new.push_str(person.imdb_id.as_str());
    let image_url = crate::extract_value_from_json_string!(response, "image");
    let extension = image_url.rsplit('.').next().unwrap();
    dir_new.push('.');
    dir_new.push_str(extension);

    person.picture_location = download_picture(String::from(image_url), dir_new).await;
    person.name = crate::extract_value_from_json_string!(response, "name").clone();

    Ok(person)
}

async fn fill_in_movie__imdb_api(mut movie: Movie, response: &JsonValue, cfg: &UserConfig, db: &str) -> Result<Movie, String> {

    movie.imdb_id = crate::extract_value_from_json_string!(response, "id").clone();

    let mut dir_new = cfg.image_dir.clone();
    if !dir_new.as_str().ends_with('/') {
        dir_new.push_str("/");
    }
    dir_new.push_str("poster_");
    dir_new.push_str(movie.imdb_id.as_str());
    let image_url = crate::extract_value_from_json_string!(response, "image");
    let extension = image_url.rsplit('.').next().unwrap();
    dir_new.push('.');
    dir_new.push_str(extension);

    movie.title = crate::extract_value_from_json_string!(response, "title").clone();
    movie.description = crate::extract_value_from_json_string!(response, "plot").clone();
    movie.imdb_rating = Some(crate::extract_value_from_json_string!(response, "imDbRating").clone());
    movie.release_year = crate::extract_value_from_json_string!(response, "year").parse::<u32>().expect("could not extract a number from year");
    movie.mins_duration = crate::extract_value_from_json_string!(response, "runtimeMins").parse::<u32>().expect("could not extract number from runtime");
    movie.genres = GenreList::from(response);

    let (director_id, director_name) = crate::extract_id_name_from_json_person_list!(response, "directorList", 0)?;
    let director = lookup_person_uncached(&String::from(director_id), &String::from("director_name"), cfg, db).await?;
    movie.director = Some(director);

    movie.poster_location = download_picture(String::from(image_url), dir_new).await;

    Ok(movie)
}

async fn lookup_person_info__imdb_api(imdb_id: &String, mut person: Person, key: &String, cfg: &UserConfig) -> Result<Person, String> {
    let client = RqClient::new();
    let imdb_url = format!("https://imdb-api.com/en/API/Name/{}/{}", key, imdb_id);
    let url = reqwest::Url::parse(imdb_url.as_str()).unwrap();

    // get json response
    println!("sending reqwest: {:?}", url);
    let res = client.get(url).send().await.unwrap();
    if res.status().is_success() {
        let body = crate::simple_error_handling!(res.text().await, String::from("problem with getting text out of IMDB's response"));
        let json: JsonValue = crate::simple_error_handling!(serde_json::from_str(body.as_str()),
                                                                                 String::from("could not convert IMDB response to json struct"));
        crate::check_response_for_errors!(json);

        // response should be OK

        person = fill_in_person__imdb_api(person, &json, cfg).await?;
        // increase person reference after it was inserted into db, not here

        Ok(person)
    } else {
        Err(format!("could not find person with id {} using imdb-api.com", imdb_id))
    }
}
pub async fn lookup_person_info(imdb_id: &String, mut opt_person: Option<Person>, api: &Api, key: &String, cfg: &UserConfig) -> Result<Person, String> {
    let mut result = opt_person.unwrap_or(Person::new());
    match api {
        Api::ImdbApi => lookup_person_info__imdb_api(imdb_id, result, key, cfg).await,
        _ => Err(String::from("datahandling::api_abstractions -> encountered an unkown api"))
    }
}

async fn lookup_movie_info__imdb_api(imdb_id: &String, mut movie: Movie, key: &String, cfg: &UserConfig, db: &str) -> Result<Movie, String> {
    // start reqwest client
    let client = RqClient::new();

    let imdb_url = format!("https://imdb-api.com/en/API/Title/{}/{}", key, imdb_id);
    let url = reqwest::Url::parse(imdb_url.as_str()).unwrap();

    // get json response
    println!("sending reqwest: {:?}", url);
    let res = client.get(url).send().await.unwrap();
    if res.status().is_success() {
        let body = crate::simple_error_handling!(res.text().await, String::from("problem with getting text out of IMDB's response"));
        let json: JsonValue = crate::simple_error_handling!(serde_json::from_str(body.as_str()), String::from("could not convert IMDB response to json struct"));
        crate::check_response_for_errors!(json);

        movie = fill_in_movie__imdb_api(movie, &json, cfg, db).await?;

        Ok(movie)
    } else {
        Err(format!("http request to IMDB returned with code {}", res.status().as_u16()))
    }
}
pub async fn lookup_movie_info(imdb_id: &String, opt_movie: Option<&Movie>, api: &Api, key: &String, cfg: &UserConfig, db: &str) -> Result<Movie, String> {
    let mut result = Movie::new();
    match api {
        Api::ImdbApi => lookup_movie_info__imdb_api(imdb_id, result, key, cfg, db).await,
        _ => Err(String::from("datahandling::api_abstractions -> encountered an unkown api"))
    }
}

async fn get_movie_json__imdb_api(imdb_id: &String, key: &String) -> Result<JsonValue, String> {
    let client = RqClient::new();

    let imdb_url = format!("https://imdb-api.com/en/API/Title/{}/{}", key, imdb_id);
    let url = reqwest::Url::parse(imdb_url.as_str()).unwrap();

    println!("sending reqwest: {:?}", url);
    let res = client.get(url).send().await.unwrap();
    if res.status().is_success() {
        let body = crate::simple_error_handling!(res.text().await, String::from("problem with getting text out of IMDB's response"));
        let json: JsonValue = crate::simple_error_handling!(serde_json::from_str(body.as_str()), String::from("could not convert IMDB response to json struct"));
        Ok(json)
    } else {
        Err(format!("http request to IMDB returned with code {}", res.status().as_u16()))
    }
}

async fn lookup_movie_id__imdb_api(title: &String, year: &String, key: &String) -> Result<String, String> {

    let client = RqClient::new();
    let imdb_url = format!("https://imdb-api.com/en/API/SearchMovie/{}/{} {}", key, title, year);

    let url = reqwest::Url::parse(imdb_url.as_str()).unwrap();

    println!("sending reqwest: {:?}", url);
    let res = client.get(url).send().await.unwrap();
    if res.status().is_success() {
        let body = crate::simple_error_handling!(res.text().await, String::from("problem with getting text out of IMDB's response"));
        let json: JsonValue = crate::simple_error_handling!(serde_json::from_str(body.as_str()),
                                                                                 String::from("could not convert IMDB response to json struct"));
        crate::check_response_for_errors!(json);
        let search_results = json.get("results").expect("did not find results").as_array().expect("json value was not an array as expected");
        if search_results.len() < 1 {
            Err(format!("did not find {}", title))
        } else {
            let first_result_id = search_results[0].get("id").expect("didn't find id in first result").as_str().unwrap();
            Ok(String::from(first_result_id))
        }
    } else {
        Err(format!("got status code {}, when looking up {}", res.status().as_u16(), title))
    }
}
pub async fn lookup_movie_id(title: &String, year: &String, api: &Api, key: &String) -> Result<String, String> {
    match api {
        Api::ImdbApi => lookup_movie_id__imdb_api(title, year, key).await,
        _ => Err(String::from("datahandling::api_abstractions -> encountered an unknown api"))
    }
}

async fn complete_movie__imdb_api(movie: &mut Movie, key: &String, db: &str, user_config: &crate::UserConfig) -> Result<(), String> {

    let mut writer_ids = Vec::new();
    let mut i: usize = 0;

    let json = get_movie_json__imdb_api(&movie.imdb_id, key).await?;

    while let Ok((writer_id, writer_name)) = crate::extract_id_name_from_json_person_list!(json, "writerList", i) {
        writer_ids.push((String::from(writer_id), String::from(writer_name)));
        i += 1;
    }
    let empty_role_list = vec![None; writer_ids.len()];

    let writer_handle = smart_insert_person_list(writer_ids, 2, &empty_role_list,
                                                 movie.id, user_config, db);

    // TODO: Music an cinematography <- these appear to be a bit more complicated:
    // might require an extra API request even

    let mut cast_pairs = Vec::new();
    i = 0;
    while let Ok((cast_id, cast_name)) = crate::extract_id_name_from_json_person_list!(json, "actorList", i) {
        cast_pairs.push((String::from(cast_id),String::from(cast_name)));
        i += 1;
    }

    let mut cast_roles = Vec::with_capacity(cast_pairs.len());
    i = 0;
    while let Ok(role) = crate::extract_role_from_json_person_list!(json, "actorList", i) {
        cast_roles.push(Some(String::from(role)));
        i += 1;
    }
    let cast_handle = smart_insert_person_list(cast_pairs, 1, &cast_roles, movie.id, user_config, db);

    movie.writers = writer_handle.await?;
    let cast = cast_handle.await?;
    movie.cast = cast_roles.iter()
                           .map(move |role_option| match role_option {
                                                      Some(role) => role.to_owned(),
                                                      None => String::new()
                                                   })
                           .zip(cast).collect();

    // at this point, if we haven't errored out, we can confidently say that movie is complete
    movie.complete = true;

    let conn = rusqlite::Connection::open(db).unwrap();
    complete_movie(&conn, &movie);

    Ok(())
}
pub async fn fill_movie_person_lists(movie: &mut Movie, key: &String, api: &Api, db: &str, user_config: &crate::UserConfig) -> Result<(), String> {
    match api {
        // complete_movie was the old name, but was already the name of the database action in
        // scope to set movie.complete to true in sqlite
        Api::ImdbApi => complete_movie__imdb_api(movie, key, db, user_config).await,
        _ => Err(format!("datahandling::api_abstractions -> encountered an unkown api"))
    }
}

async fn lookup_series_id__imdb_api(title: &String, year: &String, key: &String) -> Result<String, String> {
    // yes, I know, double code
    // TODO: clean this up

    let client = RqClient::new();
    let imdb_url = format!("https://imdb-api.com/en/API/SearchSeries/{}/{} {}", key, title, year);
    let url = reqwest::Url::parse(imdb_url.as_str()).unwrap();

    println!("sending reqwest: {:?}", url);
    let res = client.get(url).send().await.unwrap();
    if res.status().is_success() {
        let body = crate::simple_error_handling!(res.text().await, String::from("problem with getting text out of IMDB's response"));
        let json: JsonValue = crate::simple_error_handling!(serde_json::from_str(body.as_str()),
                                                                                 String::from("could not convert IMDB response to json struct"));
        crate::check_response_for_errors!(json);
        let search_results = json.get("results").expect("did not find results").as_array().expect("json value was not an array as expected");
        if search_results.len() < 1 {
            Err(format!("did not find {}", title))
        } else {
            let first_result_id = search_results[0].get("id").expect("didn't find id in first result").as_str().unwrap();
            Ok(String::from(first_result_id))
        }
    } else {
        Err(format!("got status code {}, when looking up {}", res.status().as_u16(), title))
    }
}
pub async fn lookup_series_id(title: &String, year: &String, api: &Api, key: &String) -> Result<String, String> {
    match api {
        Api::ImdbApi => lookup_series_id__imdb_api(title, year, key).await,
        _ => Err(format!("unkown api encountered"))
    }
}

async fn fill_in_series__imdb_api(response: &JsonValue, cfg: &crate::UserConfig) -> Result<Series, String> {
    let mut series = Series::new();
    series.imdb_id = crate::extract_value_from_json_string!(response, "id").clone();

    let mut dir_new = cfg.image_dir.clone();
    if !dir_new.as_str().ends_with('/') {
        dir_new.push_str("/");
    }
    dir_new.push_str("poster_");
    dir_new.push_str(series.imdb_id.as_str());
    let image_url = crate::extract_value_from_json_string!(response, "image");
    let extension = image_url.rsplit('.').next().unwrap();
    dir_new.push('.');
    dir_new.push_str(extension);

    series.title = crate::extract_value_from_json_string!(response, "title").clone();
    series.description = crate::extract_value_from_json_string!(response, "plot").clone();
    series.imdb_rating = crate::extract_value_from_json_string!(response, "imDbRating").clone();
    // start is in "year" field
    series.release_start_end[0] = crate::extract_value_from_json_string!(response, "year").parse::<u32>().expect("could not extract number for year");
    // end is in "tvSeriesInfo" > "yearEnd"
    series.release_start_end[1] = match response.get("tvSeriesInfo") {
        // if this is none, it may not be a series
        None => return Err(format!("could not find tvSeriesInfo field for series {}", series.title)),
        Some(value) => {
            match value.as_object().unwrap().get("yearEnd") {
                None => 0,
                // Ok, this is ugly:
                Some(val) => {
                    let str_val = val.as_str().unwrap();
                    if str_val.len() == 0 {
                        0
                    } else {
                        str_val.parse::<u32>().expect("could not parse json string as u32 where u32 was expected")
                    }
                }
            }
        }
    };
    series.genres = GenreList::from(response);

    series.poster_location = download_picture(String::from(image_url), dir_new).await;

    Ok(series)
}
async fn lookup_series_info__imdb_api(imdb_id: &String, key: &String, cfg: &crate::UserConfig) -> Result<Series, String> {
    // start reqwest client
    let client = RqClient::new();

    let imdb_url = format!("https://imdb-api.com/en/API/Title/{}/{}", key, imdb_id);
    let url = reqwest::Url::parse(imdb_url.as_str()).unwrap();

    // get json response
    println!("sending reqwest: {:?}", url);
    let res = client.get(url).send().await.unwrap();
    if res.status().is_success() {
        let body = crate::simple_error_handling!(res.text().await, String::from("problem with getting text out of IMDB's response"));
        let json: JsonValue = crate::simple_error_handling!(serde_json::from_str(body.as_str()), String::from("could not convert IMDB response to json struct"));
        crate::check_response_for_errors!(json);

        let series = fill_in_series__imdb_api(&json, cfg).await?;

        Ok(series)
    } else {
        Err(format!("http request to IMDB returned with code {}", res.status().as_u16()))
    }
}
pub async fn lookup_series_info(imdb_id: &String, api: &Api, key: &String, cfg: &crate::UserConfig) -> Result<Series, String> {
    match api {
        Api::ImdbApi => lookup_series_info__imdb_api(imdb_id, key, cfg).await,
        _ => Err(format!("Unkown Api"))
    }
}

async fn fill_in_episode__imdb_api(response: &JsonValue, cfg: &crate::UserConfig) -> Result<Episode, String> {
    let mut episode = Episode::new();

    episode.imdb_id = crate::extract_value_from_json_string!(response, "id").clone();
    episode.title = crate::extract_value_from_json_string!(response, "title").clone();
    episode.description = crate::extract_value_from_json_string!(response, "plot").clone();
    episode.episode_number = crate::extract_value_from_json_string!(response, "episodeNumber").parse::<i32>().expect("could not parse episode number");
    // TODO: duration_mins

    Ok(episode)
}
async fn fill_in_season__imdb_api(response: &JsonValue, cfg: &crate::UserConfig) -> Result<Season, String> {
    let mut season = Season::new();
    season.release_year = crate::extract_value_from_json_string!(response, "year").parse::<u32>().expect("could not parse year to u32");

    // iterate over episodes
    if let Some(serde_json::Value::Array(episode_array)) = response.get("episodes") {
        for episode_json in episode_array {
            let episode_json_value = JsonValue::from(episode_json.clone());
            let episode = fill_in_episode__imdb_api(&episode_json_value, cfg).await?;
            season.episodes.push(episode);
        }
        Ok(season)
    } else {
        Err(format!("could not extract episode list from json response"))
    }
}
async fn lookup_season_episodes__imdb_api(series: &Series, season_n: i32, key: &String, cfg: &crate::UserConfig) -> Result<Season, String> {
    let client = RqClient::new();

    let imdb_url = format!("https://imdb-api.com/en/API/SeasonEpisodes/{}/{}/{}", key, &series.imdb_id, &season_n);
    let url = reqwest::Url::parse(&imdb_url).unwrap();
    println!("sending reqwest: {:?}", url);
    let res = client.get(url).send().await.unwrap();
    if res.status().is_success() {
        let body = crate::simple_error_handling!(res.text().await, String::from("problem with getting text body out of imdb-api's response"));
        let json: JsonValue = crate::simple_error_handling!(serde_json::from_str(&body), String::from("could not parse response to json struct"));
        crate::check_response_for_errors!(json);

        let mut season = fill_in_season__imdb_api(&json, cfg).await?;
        season.series_imdb_id = series.imdb_id.clone();
        season.season_number = season_n;

        Ok(season)
    } else {
        Err(format!("http request to imdb-api returned with code {}", res.status().as_u16()))
    }
}
pub async fn lookup_season_episodes(series: &Series, season_n: i32, api: &Api, key: &String, cfg: &crate::UserConfig) -> Result<Season, String> {
    match api {
        Api::ImdbApi => lookup_season_episodes__imdb_api(series, season_n, key, cfg).await,
        _ => Err(format!("Unkown Api"))
    }
}
