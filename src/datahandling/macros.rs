/*********************************\
|                                 |
|   HELPER FUNCTIONS AND MACROS   |
|                                 |
\*********************************/

// macro to get a json value with useful panic! messages
// if value is not found or if the value is not a string
#[macro_export]
macro_rules! extract_value_from_json_string {
    ($data:ident, $from:expr) => {
        match $data.get($from) {
            None => return Err(format!("{} was not found in json", $from)),
            Some(value) => {
                if let serdeValue::String(val) = value {
                    val
                } else {
                    return Err(format!("found json value, {}, is not a string as expected", $from))
                }
            }
        }
    }
}
#[macro_export]
macro_rules! extract_id_name_from_json_person_list {
    ($data:ident, $from:expr, $index:expr) => {
        match $data.get($from) {
            None => return Err(format!("{} was not found in json", $from)),
            Some(value) => {
                if let Some(array) = value.as_array() {
                    if array.len() <= $index {
                        Err(format!("tried to get element {} from {}, but {} wasn't long enough", $index, $from, $from))
                    } else {
                        if let Some(id) = array[$index].get("id") {
                            if let Some(name) = array[$index].get("name") {
                                Ok((id.as_str().unwrap(), name.as_str().unwrap()))
                            } else {
                                return Err(format!("element in {} doesn't have a name as expected", $from))
                            }
                        } else {
                            return Err(format!("element in {} doesn't have an id as expected", $from))
                        }
                    }
                } else {
                    return Err(format!("{} was expected to be an array, but seems like it wasn't", $from))
                }
            }
        }
    }
}
#[macro_export]
macro_rules! extract_role_from_json_person_list {
    ($data:ident, $from:expr, $index:expr) => {
        match $data.get($from) {
            None => return Err(format!("{} was not found in json", $from)),
            Some(value) => {
                if let Some(array) = value.as_array() {
                    if array.len() <= $index {
                        Err(format!("tried to get element {} from {}, but {} wasn't long enough", $index, $from, $from))
                    } else {
                        if let Some(role) = array[$index].get("asCharacter") {
                            Ok(role.as_str().unwrap())
                        } else {
                            return Err(format!("element in {} doesn't have an id as expected", $from))
                        }
                    }
                } else {
                    return Err(format!("{} was expected to be an array, but seems like it wasn't", $from))
                }
            }
        }
    }
}

#[macro_export]
macro_rules! check_response_for_errors {
    ($data:ident) => {
        if let Some(val) = $data.get("errorMessage") {
            // received an error message from imdb
            let msg = val.as_str().unwrap();
            if msg.len() > 0 {
                return Err(String::from(msg))
            }
        }
    }
}
