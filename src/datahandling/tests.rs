use tokio::runtime::Runtime;
use std::path::Path;
use std::fs;
use crate::*;
use super::common::*;
use super::movies::*;
use super::series::*;
use super::api_abstractions;

fn init_test_db(db_name: &str) {
    if Path::new(&db_name).exists() {
        fs::remove_file(&db_name);
    }
    startup_db(db_name, 1, "./migrations");
}

fn init_test_data_dirs(image_dir: &String, video_dir: &String) {
    let full_image_dir = format!("static/{}", image_dir);
    let full_video_dir = format!("static/{}", video_dir);

    if Path::new(&full_image_dir).exists() {
        fs::remove_dir_all(&full_image_dir);
    }
    fs::create_dir_all(&full_image_dir);

    if Path::new(&full_video_dir).exists() {
        fs::remove_dir_all(&full_video_dir);
    }
    fs::create_dir_all(&full_video_dir);
}

#[test]
#[ignore]
fn test_leo() {
    // look up leonardo dicaprio to test lookup actor function
    // imdb id for leo (Leonardo DiCaprio) is nm0000138

    let mut rt = tokio::runtime::Runtime::new().unwrap(); let key = read_api_key_legacy().unwrap();
    let imdb_id = String::from("nm0000138");
    let api = api_abstractions::Api::ImdbApi;
    let key = read_api_key_legacy().unwrap();
    let mut cfg = UserConfig::default();

    let res = rt.block_on(api_abstractions::lookup_person_info(&imdb_id, None, &api, &key, &cfg));
    assert!(res.is_ok());
    let person = res.unwrap();
    assert_eq!(person.name.to_lowercase(), "leonardo dicaprio");
}

#[test]
#[ignore]
fn test_inception() {
    // lookup Inception test
    // the imdb id should be tt1375666

    let mut rt = tokio::runtime::Runtime::new().unwrap();
    let api = api_abstractions::Api::ImdbApi;
    let key = read_api_key_legacy().unwrap();
    let title = "Inception";
    let year = "2010";
    let imdb_id = String::from("tt1375666");
    let expected_director = "Christopher Nolan";
    let mut cfg = UserConfig::default();
    let db = "tests/tests_workdir/test_inception_db";
    init_test_db(db);

    let res = rt.block_on(api_abstractions::lookup_movie_info(&imdb_id, None, &api, &key, &cfg, db));
    if let Err(e) = res.as_ref() {
        println!("{}", e);
    }
    assert!(res.is_ok());

    // check if we actually fully received all correct data
    let movie = res.unwrap();
    assert_eq!(movie.imdb_id, imdb_id);
    assert_eq!(movie.title, title);

}

#[test]
#[ignore]
fn test_movie_id_lookup() {
    // only lookup Inception's imdb id
    // the imdb id should be tt1375666

    let mut rt = tokio::runtime::Runtime::new().unwrap();
    let key = read_api_key_legacy().unwrap();
    let title = String::from("Inception");
    let year = String::from("2010");
    let imdb_id = String::from("tt1375666");

    let api = api_abstractions::Api::ImdbApi;
    let res = rt.block_on(api_abstractions::lookup_movie_id(&title, &year, &api, &key));
    if let Err(e) = res.as_ref() {
        println!("{}", e);
    }
    assert!(res.is_ok());
    assert_eq!(res.unwrap(), imdb_id);
}

#[test]
#[ignore]
fn test_full_movie_handling() {
    let mut rt = tokio::runtime::Runtime::new().unwrap();
    let key = read_api_key_legacy().unwrap();
    let title = String::from("Inception");
    let year = String::from("2010");
    let mut cfg = UserConfig::default();

    cfg.video_dest = String::from("tests/tests_workdir/dest");
    cfg.image_dir = String::from("tests/tests_workdir/dest");
    cfg.api_order.push(String::from("imdb-api"));
    cfg.limit_requests = false;

    cfg = cfg.fill_in_api_keys().unwrap();

    let location = format!("{}/tests.mp4", cfg.video_dest);

    let db = "tests/tests_workdir/theatron_test_movie.db";
    init_test_db(db);

    let res = rt.block_on(smart_lookup_movie(title, year, &db, &location, &cfg));
    assert!(res.is_ok());
    let movie = res.unwrap();
    assert_eq!(movie.imdb_id, "tt1375666");
    assert!(movie.director.is_some());

    println!("movie was:\n{:?}", movie);
}

fn init_test_db_series(db_name: &str) {
    if Path::new(&db_name).exists() {
        fs::remove_file(&db_name);
    }
    startup_db(db_name, 4, "./migrations");
}

#[test]
#[ignore]
fn test_series_lookup() {
    let mut rt = tokio::runtime::Runtime::new().unwrap();
    let title = String::from("The Boys");
    let year = String::new();
    let mut cfg = UserConfig::default();

    cfg.video_dest = String::from("data/tests/dest/images");
    cfg.image_dir = String::from("data/tests/dest/images");
    init_test_data_dirs(&cfg.image_dir, &cfg.video_dest);
    cfg.api_order.push(String::from("imdb-api"));
    cfg.limit_requests = true; // TODO: also test false
    cfg = cfg.fill_in_api_keys().unwrap();

    let db = "tests/tests_workdir/theatron_test_series.db";
    init_test_db_series(db);

    let res = rt.block_on(smart_lookup_series(&title, &year, &db, &cfg));
    if let Err(e) = &res {
        println!("{}", e);
    }
    assert!(res.is_ok());
    let series = res.unwrap();
    assert_eq!(series.title, "The Boys");
    assert_eq!(series.imdb_id, "tt1190634");
}

#[test]
#[ignore]
fn test_season_lookup() {
    // first part is same as series_lookup
    let mut rt = tokio::runtime::Runtime::new().unwrap();
    let title = String::from("The Boys");
    let year = String::new();
    let mut cfg = UserConfig::default();

    cfg.video_dest = String::from("data/tests/dest/images");
    cfg.image_dir = String::from("data/tests/dest/images");
    init_test_data_dirs(&cfg.image_dir, &cfg.video_dest);
    cfg.api_order.push(String::from("imdb-api"));
    cfg.limit_requests = true; // TODO: also test false
    cfg = cfg.fill_in_api_keys().unwrap();

    let db = "tests/tests_workdir/theatron_test_series.db";
    init_test_db_series(db);

    let res = rt.block_on(smart_lookup_series(&title, &year, &db, &cfg));
    assert!(res.is_ok());
    let mut series = res.unwrap();
    // new part:
    let res = rt.block_on(series.lookup_season(1, &cfg, &db));
    assert!(res.is_ok());
    let season = res.unwrap();
    let episode1 = &season.episodes[0];
    assert_eq!(episode1.title, "The Name of the Game");
    assert_eq!(episode1.imdb_id, "tt7775902");
}

#[test]
#[ignore]
fn test_adding_episodes() {
    // first part is same as series_lookup
    let mut rt = tokio::runtime::Runtime::new().unwrap();
    let title = String::from("The Boys");
    let year = String::new();
    let mut cfg = UserConfig::default();

    cfg.video_dest = String::from("data/tests/dest/images");
    cfg.image_dir = String::from("data/tests/dest/images");
    init_test_data_dirs(&cfg.image_dir, &cfg.video_dest);
    cfg.api_order.push(String::from("imdb-api"));
    cfg.limit_requests = true; // TODO: also test false
    cfg = cfg.fill_in_api_keys().unwrap();

    let db = "tests/tests_workdir/theatron_test_episodes.db";
    init_test_db_series(db);

    let res = rt.block_on(smart_lookup_series(&title, &year, &db, &cfg));
    assert!(res.is_ok());
    let mut series = res.unwrap();
    // new part:
    let res = rt.block_on(series.lookup_season(1, &cfg, &db));
    assert!(res.is_ok());
    let mut season = res.unwrap();
    let episode_test_location = format!("{}/test_loc", cfg.video_dest);
    let res = season.add_episode(1, &episode_test_location, &cfg);
    assert!(res.is_ok());
}

// helper functions

fn read_api_key_legacy() -> Option<String> {
    // wrapper for read_api_key_legacy to work with old tests that used imdb-api only
    let api_key_map = read_api_key().unwrap();
    let api = String::from("imdb-api");
    api_key_map.get(&api).map(|e| e.clone())
}
