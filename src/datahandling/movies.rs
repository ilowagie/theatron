use super::common::*;
use super::api_abstractions;

use rocket_contrib::json::JsonValue;
use serde_json::value::Value as serdeValue;
use serde::{Serialize, Deserialize};
use crate::UserConfig;

/************\
|            |
|   STRUCTS  |
|            |
\************/

#[derive(Debug, Serialize, Deserialize)]
pub struct MovieSubtitle {
    pub id:                 i32,
    pub location:           String,
    pub language:           String
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Movie {
    pub id:                 i32,
    pub title:              String,
    pub description:        String,
    #[serde(skip)]
    pub director:           Option<Person>,
    #[serde(skip)]
    pub writers:            Vec<Person>,
    #[serde(skip)]
    pub cinematography:     Vec<Person>,
    #[serde(skip)]
    pub music:              Vec<Person>,
    #[serde(skip)]
    pub cast:               Vec<(String, Person)>, // (name in movie, ref to actor struct)
    pub genres:             GenreList,
    pub release_year:       u32,
    pub mins_duration:      u32,
    #[serde(skip, default="time::get_time")]
    pub added_at:           time::Timespec,
    pub imdb_rating:        Option<String>,
    pub movie_location:     String,
    pub poster_location:    Option<String>,
    pub imdb_id:            String,
    pub complete:           bool
}

impl Movie {
    pub fn new() -> Movie {
        // make a Movie instance that is as empty as possible
        Movie {
            id: -1, title: String::new(), description: String::new(), director: None, writers: Vec::new(),
            cinematography: Vec::new(), music: Vec::new(), cast: Vec::new(), genres: GenreList::from(Vec::new()), release_year: 1,
            mins_duration: 1, added_at: time::get_time(), imdb_rating: None, movie_location: String::new(),
            poster_location: None, imdb_id: String::new(), complete: false
        }
    }
}

pub struct CastAndCrewRelationship {
    pub id:         i32,
    pub crew_type:  u8,
    pub role:       Option<String>,
    pub movie_id:   i32,
    pub person_id:  i32
}
impl CastAndCrewRelationship {
    pub fn new() -> Self {
        CastAndCrewRelationship {
            id: -1, crew_type: 0, role: None, movie_id: -1, person_id: -1
        }
    }
}

/**********************************\
|                                  |
|   SMART INSERT/QUERY FUNCTIONS   |
|                                  |
\**********************************/

async fn insert_movie_part1(db: &str, user_config: &crate::UserConfig, movie_location: &String, imdb_id: &String) -> Result<Movie, String> {

    let mut movie_opt = None;

    for api in &user_config.api_order {
        let key = user_config.get_api_key(api)?;
        let e_api = api_abstractions::Api::from(api);

        if let Ok(movie) = api_abstractions::lookup_movie_info(imdb_id, None, &e_api, &key, user_config, db).await {
            movie_opt = Some(movie);
            break;
        }
    }

    if let Some(mut movie) = movie_opt {
        let conn = rusqlite::Connection::open(db).unwrap();

        movie.movie_location = movie_location.clone();
        insert_movie(&conn , &movie);

        Ok(movie)
    } else {
        Err(format!("couldn't find movie with id {}", imdb_id))
    }
}

// This is one of the main functions that is used to interface with the datahandling code.
// will attempt to look up movie. If it's not yet present in the database, it will be inserted
//  Err is returned if movie is not found, or there was an error in the parsing of the returned
//  json
pub async fn smart_lookup_movie(title: String, release_year: String, db: &str, location: &String, user_config: &crate::UserConfig) -> Result<Movie, String> {

    // open db connection:
    let conn = rusqlite::Connection::open(db).unwrap();

    // first, if the movie is already present in the database, just return the movie from the
    // database, no need to look it up on imdb
    if let Some(mut movie) =  find_movie_from_title_year(&conn, &title, &release_year) {


        // if found in db
        if !movie.complete && !user_config.limit_requests {

            movie = smart_complete_movie(movie, db, user_config).await?;
            movie.movie_location = location.clone(); // making sure this is correct
        }
        return Ok(movie)

    } else {

        //insert new value in db

        let id = get_imdb_movie_id(&title, &release_year, user_config).await?;
        let mut movie_query = prepare_query_movie_imdb_id(&conn);

        // just making sure: an extra db query is less expensive than an imdb api request
        if let Some(mut movie) = find_movie_from_imdb_id(&mut movie_query, &id) {
            if !movie.complete && !user_config.limit_requests {
                movie = smart_complete_movie(movie, db, user_config).await?;
                movie.movie_location = location.clone(); // making sure this is correct
            }
            return Ok(movie)
        }

        let mut movie = insert_movie_part1(db, user_config, location, &id).await?;
        movie = match find_movie_from_imdb_id(&mut movie_query, &id) {
            Some(val) => val,
            None => return Err(String::from("Could not find movie that was just inserted"))
        };

        if !user_config.limit_requests {
            movie = smart_complete_movie(movie, db, user_config).await?;
        }

        Ok(movie)
    }
}

async fn smart_complete_movie(mut movie: Movie, db: &str, user_config: &crate::UserConfig) -> Result<Movie, String> {

    for api in &user_config.api_order {
        let key = user_config.get_api_key(api)?;
        let e_api = api_abstractions::Api::from(api);

        if api_abstractions::fill_movie_person_lists(&mut movie, &key, &e_api, db, user_config).await.is_ok() {
            break;
        }
    }

    Ok(movie)
}

async fn insert_person_movie_relation_cached(person_id: &String, person_name: &String, movie_id: i32, user_config: &crate::UserConfig,
                                             person_query: &mut rusqlite::Statement<'_>,
                                             person_update: &mut rusqlite::Statement<'_>,
                                             person_insert: &mut rusqlite::Statement<'_>,
                                             crew_type: u8,
                                             role: &Option<String>,
                                             cnc_query: &mut rusqlite::Statement<'_>,
                                             cnc_insert: &mut rusqlite::Statement<'_>) -> Result<Person, String> {
    let person = lookup_person(person_id, person_name, user_config, person_query, person_update, person_insert).await?;

    if query_cnc(cnc_query, person.id).is_none() {
        // to avoid duplicates after resuming movie insertion

        let mut cnc = CastAndCrewRelationship::new();
        cnc.person_id = person.id;
        cnc.movie_id = movie_id;
        cnc.crew_type = crew_type;
        cnc.role = role.clone();

        insert_cast_and_crew_relationship(cnc_insert, cnc);
    }

    Ok(person)
}

// return list of persons
// if a person is not yet part of db, he/she will be inserted
// all persons in the list must have the same crew_type (i.e 1 -> actor, 2 -> writer,
// 3 -> cinematography, 4 -> music)
// if persons are actors, add a list of their respective roles
pub async fn smart_insert_person_list(person_pair_list: Vec<(String,String)>, crew_type: u8, roles: &Vec<Option<String>>, movie_id: i32,
                                      user_config: &crate::UserConfig, db: &str) -> Result<Vec<Person>, String> {
    let conn = rusqlite::Connection::open(db).unwrap();
    let mut person_query = prepare_query_imdb_id_of_person(&conn);
    let mut person_update = prepare_increment_reference_count(&conn);
    let mut person_insert = prepare_insert_person_stmt(&conn);
    let mut cnc_query = prepare_query_cnc(&conn, movie_id, crew_type);
    let mut cnc_insert = prepare_insert_cnc_statement(&conn, crew_type, movie_id);

    // NOTE rusqlite::Statement does not implement Send (which is logical),
    // But this means that cached statements can't be run in concurrent async tasks
    // so, what is the fastest? caching statements or running concurrent tasks?
    // I goes it depends on db maturaty and internet speed.
    // If the person does not exist in the db, a http request is made
    // So concurrency is fastest on small db with lots of requests over slow internet
    // But still something to look into
    let mut person_list = Vec::with_capacity(person_pair_list.len());
    for i in 0..person_pair_list.len() {
        let person = insert_person_movie_relation_cached(&person_pair_list[i].0,
                                                         &person_pair_list[i].1,
                                                         movie_id,
                                                         user_config,
                                                         &mut person_query,
                                                         &mut person_update,
                                                         &mut person_insert,
                                                         crew_type,
                                                         &roles[i],
                                                         &mut cnc_query,
                                                         &mut cnc_insert).await?;
        person_list.push(person);
    }

    Ok(person_list)
}

pub fn cnc_list_to_person_list(conn: &rusqlite::Connection, cnc_list: Vec<CastAndCrewRelationship>) -> Result<Vec<Person>, String> {
    let mut result = Vec::with_capacity(cnc_list.len());
    let mut query_stmt = prepare_query_id_of_person(&conn);
    for cnc in cnc_list.iter() {
        let person = query_id_of_person(&mut query_stmt, cnc.person_id)?;
        result.push(person);
    }
    Ok(result)
}

pub fn cnc_list_to_cast_list(conn: &rusqlite::Connection, cnc_list: Vec<CastAndCrewRelationship>) -> Result<Vec<(String, Person)>, String> {
    let mut result = Vec::with_capacity(cnc_list.len());
    let mut query_stmt = prepare_query_id_of_person(&conn);
    for cnc in cnc_list.iter() {
        let role = match &cnc.role {
            Some(_role) => _role.clone(),
            None => String::new() // empty string as row if no role is present in cnc, no error
        };
        let person = query_id_of_person(&mut query_stmt, cnc.person_id)?;
        result.push((role, person));
    }
    Ok(result)
}

pub fn expand_movie_info(db: &str, mut movie: Movie) -> Result<Movie, String> {
    let conn = rusqlite::Connection::open(db).unwrap();

    // writers
    let mut writers_cnc = query_cast_and_crew_relationships(&conn, 2, movie.id);
    movie.writers = cnc_list_to_person_list(&conn, writers_cnc)?;

    // cinematography
    let mut cinematography_cnc = query_cast_and_crew_relationships(&conn, 3, movie.id);
    movie.cinematography = cnc_list_to_person_list(&conn, cinematography_cnc)?;

    // music
    let mut music_cnc = query_cast_and_crew_relationships(&conn, 4, movie.id);
    movie.music = cnc_list_to_person_list(&conn, music_cnc)?;

    // cast
    let mut cast_cnc = query_cast_and_crew_relationships(&conn, 1, movie.id);
    movie.cast = cnc_list_to_cast_list(&conn, cast_cnc)?;

    Ok(movie)
}

/**************************\
|                          |
|   IMDB-API INTERACTION   |
|                          |
\**************************/

// lookup movie id using the imdb search API
async fn get_imdb_movie_id(title: &String, release_year: &String, user_config: &crate::UserConfig) -> Result<String, String> {
    for api in &user_config.api_order {
        let key = user_config.get_api_key(api)?;
        let e_api = api_abstractions::Api::from(api);
        if let Ok(id) = api_abstractions::lookup_movie_id(title, release_year, &e_api, &key).await {
            return Ok(id);
        }
    }

    Err(format!("could not find id for {} - {}", title, release_year))
}

/***********************************\
|                                   |
|   DUMB DATABASE INSERTS/UPDATES   |
|                                   |
\***********************************/

fn insert_movie(conn: &rusqlite::Connection, movie: &Movie) {
    let mut stmt = conn.prepare("insert into movies (title,
                                             description,
                                             director_id,
                                             genres,
                                             release_year,
                                             mins_duration,
                                             added_at,
                                             imdb_rating,
                                             movie_location,
                                             poster_location,
                                             imdb_id,
                                             complete)
                                        values (:title,
                                                :description,
                                                :director,
                                                :genres,
                                                :release_year,
                                                :mins_duration,
                                                :added_at,
                                                :imdb_rating,
                                                :movie_location,
                                                :poster_location,
                                                :imdb_id,
                                                :complete)")
                       .unwrap();
    let director_id = match &movie.director {
        Some(person) => person.id,
        None => 0
    };
    stmt.execute_named(&[(":title", &movie.title),
                         (":description", &movie.description),
                         (":director", &director_id),
                         (":genres", &movie.genres),
                         (":release_year", &movie.release_year),
                         (":mins_duration", &movie.mins_duration),
                         (":added_at", &movie.added_at),
                         (":imdb_rating", &movie.imdb_rating),
                         (":movie_location", &movie.movie_location),
                         (":poster_location", &movie.poster_location),
                         (":imdb_id", &movie.imdb_id),
                         (":complete", &movie.complete)]).unwrap();
}

pub fn complete_movie(conn: &rusqlite::Connection, movie: &Movie) {
    conn.execute("update movies set complete=?1 where movie_id=?2", &[&movie.complete, &movie.id]).unwrap();
}

// to be called in scrubber.rs
pub fn add_subtitle(db: &str, movie_id: i32, sub: MovieSubtitle) {
    let conn = rusqlite::Connection::open(db).unwrap();

    let mut stmt = conn.prepare("insert into movie_subs
                                           (file_location,
                                            language,
                                            movie_id)
                                        values (:file_location,
                                                :language,
                                                :movie_id)")
                       .unwrap();

    stmt.execute_named(&[(":file_location", &sub.location),
                         (":language", &sub.language),
                         (":movie_id", &movie_id)])
        .unwrap();
}

/**********************\
|                      |
|   DATABASE QUERIES   |
|                      |
\**********************/

// helper: lazy movie builder
fn make_movie(row: &rusqlite::Row) -> Option<Movie> {
    let director = Person {
        id: row.get(12),
        name: row.get(13),
        referenced: row.get(14),
        picture_location: row.get(15),
        imdb_id: row.get(16)
    };
    Some(Movie {
        id: row.get(0),
        title: row.get(1),
        description: row.get(2),
        director: Some(director),
        writers: Vec::new(),
        cinematography: Vec::new(),
        music: Vec::new(),
        cast: Vec::new(),
        genres: row.get(3),
        release_year: row.get(4),
        mins_duration: row.get(5),
        added_at: row.get(6),
        imdb_rating: row.get(7),
        movie_location: row.get(8),
        poster_location: row.get(9),
        imdb_id: row.get(10),
        complete: row.get(11)
    })
}

static LAZY_MOVIE_SELECT_STR: &str = "select m.movie_id, m.title, m.description, m.genres, m.release_year, m.mins_duration,
                                      m.added_at, m.imdb_rating, m.movie_location, m.poster_location, m.imdb_id, m.complete,
                                      d.person_id, d.name, d.referenced, d.picture_location, d.imdb_id
                                      from movies m join people d on
                                      m.director_id = d.person_id";

pub fn get_movie_from_local_id(conn: &rusqlite::Connection, id: i32) -> Result<Movie, String> {
    conn.query_row(format!("{} where m.movie_id=?1", LAZY_MOVIE_SELECT_STR).as_str(), &[&id], |row| make_movie(row).unwrap())
        .map_err(|_| format!("encountered error while looking up movie with id {}", id))
}

pub fn query_movies_lazy(conn: &rusqlite::Connection, ordering: &DataOrder, page_n: i32, n_per_page: i32) -> Vec<Movie> {
    let offset = page_n * n_per_page;
    let mut stmt = conn.prepare(format!("{}
                                         {}
                                         limit :n_per_page offset :offset",
                                         LAZY_MOVIE_SELECT_STR,
                                         ordering.as_string_for_movies()
                                       ).as_str()).unwrap();
    stmt.query_map_named(&[(":n_per_page", &n_per_page), (":offset", &offset)],
                         |row| make_movie(row)).unwrap()
                         .filter_map(|elem| elem.unwrap())
                         .collect()
}

fn prepare_query_movie_imdb_id<'clf>(conn: &'clf rusqlite::Connection) -> rusqlite::Statement<'clf> {
    conn.prepare("select m.movie_id, m.title, m.description, m.genres, m.release_year, m.mins_duration,
                  m.added_at, m.imdb_rating, m.movie_location, m.poster_location, m.imdb_id, m.complete,
                  d.person_id, d.name, d.referenced, d.picture_location, d.imdb_id
                  from movies m join people d on
                  m.director_id = d.person_id
                  where m.imdb_id = :imdb_id").unwrap()

}
// returns Some(lazy_movie) based on IMDB id
fn find_movie_from_imdb_id(stmt: &mut rusqlite::Statement, imdb_id: &String) -> Option<Movie> {
    let mut rows = stmt.query_named(&[(":imdb_id", &imdb_id.as_str())]).unwrap();
    if let Some(result_row) = rows.next() {
        let row = result_row.unwrap();
        make_movie(&row)
    } else {
        None
    }
}

// returns Some(lazy_movie) if title and year combination is found in db
fn find_movie_from_title_year(conn: &rusqlite::Connection, title: &String, year: &String) -> Option<Movie> {
    let mut stmt = conn.prepare("select m.movie_id, m.title, m.description, m.genres, m.release_year, m.mins_duration,
                                 m.added_at, m.imdb_rating, m.movie_location, m.poster_location, m.imdb_id, m.complete,
                                 d.person_id, d.name, d.referenced, d.picture_location, d.imdb_id
                                 from movies m join people d on
                                 m.director_id = d.person_id
                                 where m.title like :title and m.release_year = :year").unwrap();
    let mut rows = stmt.query_named(&[(":title", &title.as_str()), (":year", &year.as_str())]).unwrap();
    if let Some(result_row) = rows.next() {
        let row = result_row.unwrap();
        make_movie(&row)
    } else {
        None
    }
}

pub fn find_all_subtitles_for_movie(conn: &rusqlite::Connection, movie_id: i32) -> Vec<MovieSubtitle> {
    let mut stmt = conn.prepare("select sub_id, file_location, language
                                 from movie_subs
                                 where movie_id = :movie_id").unwrap();
    stmt.query_map_named(&[(":movie_id", &movie_id)],
                         |row| Some(MovieSubtitle { id: row.get(0),
                                                    location: row.get(1),
                                                    language: row.get(2) })
                        ).unwrap().filter_map(|elem| elem.unwrap()).collect()
}

pub fn query_cast_and_crew_relationships(conn: &rusqlite::Connection, crew_type: u8, movie_id: i32) -> Vec<CastAndCrewRelationship> {
    let mut stmt = conn.prepare(format!("select cnc_id, type, role, movie_id, person_id
                                         from cast_and_crew
                                         where type = {}
                                         and movie_id = {}", crew_type, movie_id).as_str())
                                .unwrap();
    let mut rows = stmt.query(&[]).unwrap();
    let mut result = Vec::new();
    while let Some(Ok(row)) = rows.next() {
        let mut rel = CastAndCrewRelationship {
            id: row.get(0),
            crew_type: row.get(1),
            role: row.get(2),
            movie_id: row.get(3),
            person_id: row.get(4)
        };
        result.push(rel);
    }

    result
}

pub fn prepare_query_cnc<'clf>(conn: &'clf rusqlite::Connection, movie_id: i32, crew_type: u8) -> rusqlite::Statement<'clf> {
    conn.prepare(format!("select cnc_id, type, role, movie_id, person_id
                          from cast_and_crew
                          where type = {}
                          and movie_id = {}
                          and person_id = :person_id", crew_type, movie_id).as_str()).unwrap()
}
pub fn query_cnc(stmt: &mut rusqlite::Statement, person_id: i32) -> Option<CastAndCrewRelationship> {
    let mut rows = stmt.query_named(&[(":person_id", &person_id)]).unwrap();
    if let Some(Ok(row)) = rows.next() {
        Some(CastAndCrewRelationship {
            id: row.get(0),
            crew_type: row.get(1),
            role: row.get(2),
            movie_id: row.get(3),
            person_id: row.get(4)
        })
    } else {
        None
    }
}

// careful: this produces a named statement
//          the names are the same as the names of the members of the struct
pub fn prepare_insert_cnc_statement<'clf>(conn: &'clf rusqlite::Connection, crew_type: u8, movie_id: i32) -> rusqlite::Statement<'clf> {
    conn.prepare(format!("insert into cast_and_crew (type,
                                                     role,
                                                     movie_id,
                                                     person_id)
                          values ({},
                                  :role,
                                  {},
                                  :person_id)", crew_type, movie_id)
        .as_str()).unwrap()
}
pub fn insert_cast_and_crew_relationship(stmt: &mut rusqlite::Statement, cnc: CastAndCrewRelationship) {
    stmt.execute_named(&[(":role", &cnc.role),
                         (":person_id", &cnc.person_id)]).unwrap();
}

