use super::common::*;
use super::api_abstractions;

use std::path::Path;
use std::fs;
use rocket_contrib::databases::rusqlite;
use std::convert::TryInto;
use rocket_contrib::json::JsonValue;
use serde_json::value::Value as serdeValue;
use serde::{Serialize, Deserialize};
use crate::UserConfig;

/************\
|            |
|   STRUCTS  |
|            |
\************/

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Episode {
    pub id: i32,
    pub title: String,
    pub description: String,
    pub duration_mins: u32,
    pub imdb_id: String,

    pub episode_number: i32,

    #[serde(skip)]
    pub writers: Vec<Person>,
    #[serde(skip)]
    pub directors: Vec<Person>,
    #[serde(skip)]
    pub episode_cast: Vec<Person>,
    #[serde(skip, default="time::get_time")]
    pub added_at: time::Timespec,

    pub is_complete: bool,
    pub is_present: bool,
    pub location: String
}
impl Episode {
    pub fn new() -> Self {
        Episode {
            id: 0, title: String::new(), description: String::new(), duration_mins: 0,
            imdb_id: String::new(), episode_number: 0,
            writers: Vec::new(), directors: Vec::new(), episode_cast: Vec::new(), added_at: time::get_time(),
            is_complete: false, is_present: false, location: String::new()
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Season {
    // when there is reason to assume that a certain season is present,
    // request data of the season and save all episodes, (1 request with imdb-api)
    // are there movie files for every episode, set full_season_present to true

    pub id: i32,
    pub release_year: u32,
    // seasons don't have an imdb id, episodes do

    // finding series in db without db id is done with season_number + series_imdb_id
    pub season_number: i32,
    pub series_imdb_id: String,

    pub episodes: Vec<Episode>,
    #[serde(skip, default="time::get_time")]
    pub added_at: time::Timespec,

    pub full_season_present: bool,
    pub full_season_complete: bool,
}
impl Season {
    pub fn new() -> Self {
        Season {
            id: 0, release_year: 0,
            season_number: 0, series_imdb_id: String::new(),
            episodes: Vec::new(), added_at: time::get_time(),
            full_season_present: false, full_season_complete: false
        }
    }

    pub fn add_episode(&mut self, episode_n: i32, location: &String, user_config: &crate::UserConfig) -> Result<(), String> {
        // TODO: when looking up a season, the episodes' duration is not given
        //       so it may be better to find the duration from the video file we have than to make
        //       a new external request for every episode
        if let Some(mut episode) = self.episodes.iter_mut().find(|ep| ep.episode_number == episode_n) {
            episode.location = location.clone();
            episode.is_present = true;
            if !episode.is_complete && !user_config.limit_requests {
                todo!();
            }
            Ok(())
        } else {
            Err(format!("episode was not yet present in series, something has gone wrong, maybe episode is not part of this series or season?"))
        }
    }

    pub async fn construct_basic(series: &Series, season_n: i32, user_config: &crate::UserConfig) -> Result<Self, String> {
        let mut season_opt = None;

        for api in &user_config.api_order {
            let key = user_config.get_api_key(api)?;
            let e_api = api_abstractions::Api::from(api);

            let cur_res = api_abstractions::lookup_season_episodes(series, season_n, &e_api, &key, user_config).await;
            if let Ok(season) = cur_res {
                season_opt = Some(season);
                break;
            } else {
                println!("encountered error {}", cur_res.err().unwrap());
            }
        }

        if let Some(season) = season_opt {
            Ok(season)
        } else {
            Err(format!("haven't find season {}", season_n))
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Series {
    pub id: i32,
    pub title: String,
    pub description: String,
    pub imdb_rating: String,
    pub release_start_end: Vec<u32>, // has length 1 if still running, else second value is last year
    pub poster_location: Option<String>,
    pub imdb_id: String,
    pub average_ep_duration_mins: u32,
    pub genres: GenreList,

    #[serde(skip)]
    pub main_cast: Vec<Person>,
    #[serde(skip)]
    pub creators: Vec<Person>,
    #[serde(skip)]
    pub seasons: Vec<Season>,
    #[serde(skip)]
    calculated_avg_season: u32, // for average_ep_duration_mins update
    #[serde(skip, default="time::get_time")]
    pub added_at: time::Timespec,
    pub complete: bool
}
impl Series {
    pub fn new() -> Self {
        Series {
            id: 0, title: String::new(), description: String::new(), imdb_rating: String::new(), release_start_end: vec![0, 0],
            poster_location: None, imdb_id: String::new(), average_ep_duration_mins: 0, genres: GenreList::from(Vec::new()),
            main_cast: Vec::new(), creators: Vec::new(), seasons: Vec::new(), added_at: time::get_time(), calculated_avg_season: 0,
            complete: false
        }
    }
    pub fn update_avg_duration(&mut self) {
        let mut sum: u32 = self.average_ep_duration_mins * self.calculated_avg_season;
        let season_n: u32 = self.seasons.len().try_into().unwrap();
        for season in self.seasons.iter().skip(self.calculated_avg_season.try_into().unwrap()) {
            let mut season_duration_sum: u32 = 0;
            let episode_n: u32 = season.episodes.len().try_into().unwrap();

            for episode in &season.episodes {
                season_duration_sum += episode.duration_mins;
            }

            sum += (season_duration_sum / episode_n);
            self.calculated_avg_season += 1;
        }
        self.average_ep_duration_mins = sum / season_n;
    }

    // takes ownership of self which is not returned,
    //      the reason for this is that changes can be made to the returned Season, which could
    //      make self incorrect
    pub async fn lookup_season(mut self, season_n: i32, user_config: &crate::UserConfig, db: &str) -> Result<Season, String> {
        if let Some(mut season) = self.seasons.iter_mut().find(|seas| seas.season_number == season_n) {
            // season is already known and added
            if !user_config.limit_requests && !season.episodes[0].is_complete {
                todo!();
            }
            Ok(season.clone())
        } else {
            // season needs to be added
            let mut season = Season::construct_basic(&self, season_n, user_config).await?;
            if !user_config.limit_requests {
                todo!()
            }
            let conn = rusqlite::Connection::open(db).map_err(|e| format!("{}", e))?;
            insert_season_series(&season, &self, &conn)?;
            Ok(season)
        }
    }
}

struct SeriesCastAndCrewRelationship {
    pub id:             i32,
    // crew_type:
    //  0 = undefined
    //  1 = writer
    //  2 = director
    //  3 = cast
    //  4 = creator
    pub crew_type:      u8,
    // relation_type:
    //  0 = undefined
    //  1 = product_id is an episode id
    //  2 = product_id is a series id
    pub relation_type:  u8,
    pub role:           Option<String>,
    pub product_id:     i32,
    pub person_id:      i32
}
impl SeriesCastAndCrewRelationship {
    pub fn new() -> Self {
        SeriesCastAndCrewRelationship {
            id: -1, crew_type: 0, relation_type: 0, role: None, product_id: -1, person_id: -1
        }
    }
}

/*****************\
|                 |
|   INTERFACING   |
|                 |
\*****************/

pub async fn smart_lookup_series(title: &String, year: &String, db: &str, user_config: &crate::UserConfig) -> Result<Series, String> {
    let conn = rusqlite::Connection::open(db).map_err(|e| format!("{}", e))?;

    if let Some(mut series) = query_series_from_title(title, year, &conn)? {
        // series is present in database
        if !series.complete && !user_config.limit_requests {
            todo!();
        }
        Ok(series)
    } else {
        // series needs to be added to the database first

        let imdb_id = get_series_imdb_id(title, year, user_config).await?;

        // extra db query is less expensive than an extra api request
        if let Some(mut series) = query_series_from_imdb_id(db, &imdb_id)? {
            // series was already present, complete it if needed and return
            if !series.complete && !user_config.limit_requests {
                todo!();
            }
            Ok(series)
        } else {
            // now we're sure that the series was not yet added to the database

            let mut series = construct_basic_series(db, user_config, &imdb_id).await?;

            if !user_config.limit_requests {
                todo!()
            }

            Ok(series)
        }
    }
}
async fn get_series_imdb_id(title: &String, year: &String, user_config: &crate::UserConfig) -> Result<String, String> {
    for api in &user_config.api_order {
        let key = user_config.get_api_key(api)?;
        let e_api = api_abstractions::Api::from(api);
        if let Ok(id) = api_abstractions::lookup_series_id(title, year, &e_api, &key).await {
            return Ok(id);
        }
    }
    Err(format!("could not find id for {} - {}", title, year))
}
async fn construct_basic_series(db: &str, user_config: &crate::UserConfig, imdb_id: &String) -> Result<Series, String> {
    let mut series_opt = None;

    for api in &user_config.api_order {
        let key = user_config.get_api_key(api)?;
        let e_api = api_abstractions::Api::from(api);

        let cur_res = api_abstractions::lookup_series_info(imdb_id, &e_api, &key, user_config).await;
        if let Ok(series) = cur_res {
            series_opt = Some(series);
            break;
        } else {
            println!("encountered error {}", cur_res.err().unwrap());
        }
    }

    if let Some(mut series) = series_opt {
        series.id = insert_series(&series, db)?;
        Ok(series)
    } else {
        Err(format!("couldn't find series with imdb_id: {}", imdb_id))
    }
}

/*********************\
|                     |
|   DATABASE ACCESS   |
|                     |
\*********************/

// returns id of just inserted series
fn insert_series(series: &Series, db: &str) -> Result<i32, String> {
    let conn = rusqlite::Connection::open(db).map_err(|e| format!("{}", e))?;

    conn.execute_named("insert into series (
                            title,
                            description,
                            imdb_rating,
                            release_start,
                            release_end,
                            poster_location,
                            imdb_id,
                            avg_ep_duration_mins,
                            genres,
                            calc_avg_season,
                            added_at,
                            complete
                        ) values (
                            :title,
                            :description,
                            :imdb_rating,
                            :release_start,
                            :release_end,
                            :poster_location,
                            :imdb_id,
                            :average_ep_duration_mins,
                            :genres,
                            :calculated_avg_season,
                            :added_at,
                            :complete
                        )",
                        &[
                            (":title", &series.title),
                            (":description", &series.description),
                            (":imdb_rating", &series.imdb_rating),
                            (":release_start", &series.release_start_end[0]),
                            (":release_end", &series.release_start_end[1]),
                            (":poster_location", &series.poster_location),
                            (":imdb_id", &series.imdb_id),
                            (":average_ep_duration_mins", &series.average_ep_duration_mins),
                            (":genres", &series.genres),
                            (":calculated_avg_season", &series.calculated_avg_season),
                            (":added_at", &series.added_at),
                            (":complete", &series.complete)
                        ]).map_err(|e| format!("{}", e))?;

    let series_id: i32 = conn.query_row("select series_id from series where imdb_id = ?1",
                                         &[&series.imdb_id],
                                         |row| row.get(0)).map_err(|e| format!("{}", e))?;
    if series.seasons.len() > 0 {
        // add series seasons
        for season in &series.seasons {
            let season_id = insert_season(season, &conn)?;
            conn.execute_named("insert into series_seasons_rel (
                                    series_id,
                                    season_id
                                ) values (
                                    :series_id,
                                    :season_id
                                )",
                               &[(":series_id", &series_id),
                                 (":season_id", &season_id)])
                .map_err(|e| format!("{}", e))?;
        }
    }

    Ok(series_id)
}

fn make_series_from_row(row: &rusqlite::Row) -> Series {
    Series {
        id: row.get(0),
        title: row.get(1),
        description: row.get(2),
        imdb_rating: row.get(3),
        release_start_end: vec![row.get(4), row.get(5)],
        poster_location: row.get(6),
        imdb_id: row.get(7),
        average_ep_duration_mins: row.get(8),
        genres: row.get(9),
        calculated_avg_season: row.get(10),
        added_at: row.get(11),
        complete: row.get(12),
        main_cast: Vec::new(),
        creators: Vec::new(),
        seasons: Vec::new()
    }
}
fn add_episodes_to_series(series: &mut Series, conn: &rusqlite::Connection) -> Result<(), String> {
    let mut related_episode_query_stmt = conn.prepare("select season_id
                                                       from series_seasons_rel
                                                       where series_id = ?1")
                                             .map_err(|e| format!("{}", e))?;
    let rows = related_episode_query_stmt.query_map(&[&series.id],
                                                    |row| query_season_from_id(conn, &row.get(0)))
                                         .map_err(|e| format!("{}", e))?;

    // TODO: better error handling needed maybe, this is a bit disgusting
    series.seasons = rows.filter_map(|elem| if elem.is_err() { None } else { Some(elem.unwrap()) }) // filter away the rusqlite errors of row queries
                         .filter_map(|elem| if elem.is_err() { None } else { Some(elem.unwrap()) }) // filter away the errors from query_season_from_id
                         .collect();

    Ok(())
}
fn query_series_from_imdb_id(db: &str, imdb_id: &String) -> Result<Option<Series>, String> {
    let conn = rusqlite::Connection::open(db).map_err(|e| format!("{}", e))?;
    let query_result = conn.query_row("select series_id, title, description, imdb_rating, release_start, release_end,
                                              poster_location, imdb_id, avg_ep_duration_mins, genres, calc_avg_season, added_at,
                                              complete
                                       from series where imdb_id = ?1", &[imdb_id],
                                      |row| make_series_from_row(&row));
    match query_result {
        Ok(mut series) => {
            add_episodes_to_series(&mut series, &conn)?;
            Ok(Some(series))
        },
        Err(rusqlite::Error::QueryReturnedNoRows) => {
            Ok(None)
        },
        Err(e) => Err(format!("{}", e))
    }
}
fn query_series_from_title(title: &String, year: &String, conn: &rusqlite::Connection) -> Result<Option<Series>, String> {
    // must allow year to be an empty string
    // as it will not be present in all files and can be a rather anoying thing to add manually
    let query_result = if year.len() > 0 {
        // year is included into the query
        conn.query_row("select series_id, title, description, imdb_rating, release_start, release_end,
                               poster_location, imdb_id, avg_ep_duration_mins, genres, calc_avg_season, added_at,
                               complete
                        from series where title = ?1 and year = ?2", &[title, year],
                       |row| make_series_from_row(&row))
    } else {
        conn.query_row("select series_id, title, description, imdb_rating, release_start, release_end,
                               poster_location, imdb_id, avg_ep_duration_mins, genres, calc_avg_season, added_at,
                               complete
                        from series where title = ?1", &[title],
                       |row| make_series_from_row(&row))
    };
    match query_result {
        Ok(mut series) => {
            add_episodes_to_series(&mut series, &conn)?;
            Ok(Some(series))
        },
        Err(rusqlite::Error::QueryReturnedNoRows) => {
            Ok(None)
        },
        Err(e) => Err(format!("{}", e))
    }
}

// returns id of inserted season
fn insert_season(season: &Season, conn: &rusqlite::Connection) -> Result<i32, String> {
    conn.execute_named("insert into seasons (
                            release_year,
                            season_number,
                            series_imdb_id,
                            added_at,
                            full_season_present,
                            full_season_complete
                        ) values (
                            :release_year,
                            :season_number,
                            :series_imdb_id,
                            :added_at,
                            :full_season_present,
                            :full_season_complete
                        )",
                        &[
                            (":release_year", &season.release_year),
                            (":season_number", &season.season_number),
                            (":series_imdb_id", &season.series_imdb_id),
                            (":added_at", &season.added_at),
                            (":full_season_present", &season.full_season_present),
                            (":full_season_complete", &season.full_season_complete)
                        ]).map_err(|e| format!("{}", e))?;
    let season_id: i32 = conn.query_row("select season_id from seasons where series_imdb_id = ?1 and season_number = ?2",
                                         &[&season.series_imdb_id, &season.season_number],
                                         |row| row.get(0)).map_err(|e| format!("{}", e))?;

    if season.episodes.len() > 0 {
        let mut insert_stmt = prepare_insert_episode(&conn)?;
        // add episodes
        for episode in &season.episodes {
            let episode_id = insert_episode(&conn, &mut insert_stmt, episode)?;
            conn.execute_named("insert into seasons_episodes_rel (
                                    season_id,
                                    episode_id
                                ) values (
                                    :season_id,
                                    :episode_id
                                )",
                               &[(":season_id", &season_id),
                                 (":episode_id", &episode_id)])
                .map_err(|e| format!("{}", e))?;
        }
    }

    Ok(season_id)
}
fn insert_season_series(season: &Season, series: &Series, conn: &rusqlite::Connection) -> Result<(), String> {
    let season_id = insert_season(season, conn)?;
    conn.execute_named("insert into series_seasons_rel (
                            series_id,
                            season_id
                        ) values (
                            :series_id,
                            :season_id
                        )",
                       &[(":series_id", &series.id),
                         (":season_id", &season_id)])
        .map_err(|e| format!("{}", e))?;
    Ok(())
}

fn make_season_from_row(row: &rusqlite::Row) -> Season {
    Season {
        id: row.get(0),
        release_year: row.get(1),
        season_number: row.get(2),
        series_imdb_id: row.get(3),
        added_at: row.get(4),
        full_season_present: row.get(5),
        full_season_complete: row.get(6),
        episodes: Vec::new()
    }
}
fn query_season_from_id(conn: &rusqlite::Connection, id: &i32) -> Result<Season, String> {
    let mut season = conn.query_row("select season_id, release_year, season_number, series_imdb_id,
                                            added_at, full_season_present, full_season_complete
                                     from seasons where season_id = ?1",
                                    &[id], |row| make_season_from_row(&row))
                         .map_err(|e| format!("{}", e))?;

    // now search the season's episodes

    let mut related_episode_query_stmt = conn.prepare("select e.episode_id, e.title, e.description, e.duration_mins, e.imdb_id, e.episode_number,
                                                              e.added_at, e.is_complete, e.is_present, e.location
                                                       from seasons_episodes_rel rel
                                                       inner join episodes e on
                                                           e.episode_id = rel.episode_id
                                                       where rel.season_id = ?1")
                                             .map_err(|e| format!("{}", e))?;
    let rows = related_episode_query_stmt.query_map(&[&season.id],
                                                    |row| make_episode_from_row(&row))
                                         .map_err(|e| format!("{}", e))?;

    // TODO maybe handle these row errors better, but I don't really expect ones here...
    season.episodes = rows.filter_map(|elem| if elem.is_err() { None } else { Some(elem.unwrap()) })
                          .collect();

    Ok(season)
}

// episodes will most likely be inserted one after the other for a whole season, so caching of
// sqlite statement would be handy
fn prepare_insert_episode<'clf>(conn: &'clf rusqlite::Connection) -> Result<rusqlite::Statement<'clf>, String> {
    conn.prepare("insert into episodes (
                    title,
                    description,
                    duration_mins,
                    imdb_id,
                    episode_number,
                    added_at,
                    is_complete,
                    is_present,
                    location
                  ) values (
                    :title,
                    :description,
                    :duration_mins,
                    :imdb_id,
                    :episode_number,
                    :added_at,
                    :is_complete,
                    :is_present,
                    :location
                  )").map_err(|e| format!("{}", e))
}
fn insert_episode(conn: &rusqlite::Connection, stmt: &mut rusqlite::Statement, episode: &Episode) -> Result<i32, String> {
    stmt.execute_named(&[
                        (":title", &episode.title),
                        (":description", &episode.description),
                        (":duration_mins", &episode.duration_mins),
                        (":imdb_id", &episode.imdb_id),
                        (":episode_number", &episode.episode_number),
                        (":added_at", &episode.added_at),
                        (":is_complete", &episode.is_complete),
                        (":is_present", &episode.is_present),
                        (":location", &episode.location)
                       ]).map_err(|e| format!("{}", e))?;

    conn.query_row("select episode_id from episodes where imdb_id = ?1",
                   &[&episode.imdb_id],
                   |row| row.get(0)).map_err(|e| format!("{}", e))
}

fn make_episode_from_row(row: &rusqlite::Row) -> Episode {
    Episode {
        id: row.get(0),
        title: row.get(1),
        description: row.get(2),
        duration_mins: row.get(3),
        imdb_id: row.get(4),
        episode_number: row.get(5),
        added_at: row.get(6),
        is_complete: row.get(7),
        is_present: row.get(8),
        location: row.get(9),
        directors: Vec::new(),
        episode_cast: Vec::new(),
        writers: Vec::new()
    }
}
fn query_episode_from_imdb_id(conn: &rusqlite::Connection, imdb_id: &String) -> Result<Episode, String> {
    conn.query_row_named("select episode_id, title, description, duration_mins, imdb_id, episode_number,
                                 added_at, is_complete, is_present, location
                          from episodes where imdb_id = :imdb_id",
                          &[(":imdb_id", imdb_id)],
                          |row| make_episode_from_row(&row))
        .map_err(|e| format!("{}", e))
}

/*
 *  tests for database manipulation functions
 */

fn init_test_db(db_name: &str) {
    if Path::new(&db_name).exists() {
        fs::remove_file(&db_name);
    }
    startup_db(db_name, 4, "./migrations");
}

#[test]
fn test_db_series() {
    let db = "tests/tests_workdir/series_tests.db";
    init_test_db(&db);

    let mut series = Series::new();
    series.imdb_id = String::from("testid");
    series.title = String::from("test series 1");
    series.description = String::from("a test series");
    series.release_start_end[0] = 2010;
    series.release_start_end[1] = 2011;

    assert!(insert_series(&series, &db).is_ok());

    let queried_series = query_series_from_imdb_id(&db, &series.imdb_id).unwrap().unwrap();

    assert_eq!(series.imdb_id, queried_series.imdb_id);
    assert_eq!(series.title, queried_series.title);
    assert_eq!(series.description, queried_series.description);
    assert_eq!(series.release_start_end, queried_series.release_start_end);
}

#[test]
fn test_db_season() {
    let db = "tests/tests_workdir/season_tests.db";
    init_test_db(&db);
    let conn = rusqlite::Connection::open(&db).unwrap();

    let mut season = Season::new();
    season.release_year = 2010;
    season.season_number = 1;
    season.series_imdb_id = String::from("test id");

    let id = insert_season(&season, &conn).unwrap();
    let queried_season = query_season_from_id(&conn, &id).unwrap();

    assert_eq!(season.release_year, queried_season.release_year);
    assert_eq!(season.season_number, queried_season.season_number);
    assert_eq!(season.series_imdb_id, queried_season.series_imdb_id);
}

#[test]
fn test_db_episode() {
    let db = "tests/tests_workdir/episode_tests.db";
    init_test_db(&db);

    let mut episode = Episode::new();
    episode.title = String::from("test episode");
    episode.imdb_id = String::from("test id");
    episode.episode_number = 1;

    let conn = rusqlite::Connection::open(&db).unwrap();
    let mut insert_stmt = prepare_insert_episode(&conn).unwrap();
    assert!(insert_episode(&conn, &mut insert_stmt, &episode).is_ok());

    let queried_episode = query_episode_from_imdb_id(&conn, &episode.imdb_id).unwrap();

    assert_eq!(queried_episode.title, episode.title);
    assert_eq!(queried_episode.imdb_id, episode.imdb_id);
    assert_eq!(queried_episode.episode_number, episode.episode_number);
}
