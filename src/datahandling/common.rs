use super::api_abstractions;

use rocket_contrib::databases::rusqlite;
use rusqlite::types::{ToSqlOutput, ToSql, FromSql, FromSqlResult, FromSqlError};
use rocket_contrib::json::JsonValue;
use serde_json::value::Value as serdeValue;
use std::result::Result;
use std::process::Command;
use std::convert::{Into, From};
use reqwest::Client as RqClient;
use tokio::task;
use std::path::Path;
use std::fs::read;

use serde::{Serialize, Deserialize};

#[database("theatron_db")]
pub struct TheatronDB(rusqlite::Connection);

/*********************************\
|                                 |
|   CONVOLUTED GenreList STRUCT   |
|                                 |
\*********************************/

// this sux
#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct GenreList {
    content: Vec<String>
}
impl From<Vec<String>> for GenreList {
    fn from(str_vec: Vec<String>) -> Self {
        GenreList {
            content: str_vec
        }
    }
}
impl From<&JsonValue> for GenreList {
    fn from(full_json: &JsonValue) -> Self {
        let genre_array = full_json.get("genreList").expect("didn't find genre list in json").as_array().unwrap();
        let string_vec: Vec<String> = genre_array.iter()
                                                 .map(|val| String::from(val.get("value").expect("genre didn't have value").as_str().unwrap()))
                                                 .collect();
        GenreList::from(string_vec)
    }
}
impl From<&GenreList> for Vec<String> {
    fn from(list: &GenreList) -> Vec<String> {
        list.content.clone()
    }
}

// implementing the conversion for an array of strings from and to sql
// this is why Vec<String> is encapsulated in the GenreList struct
// You can't implement an trait from an other crate for an arbitrary type
impl FromSql for GenreList {
    fn column_result(value: rusqlite::types::ValueRef<'_>) -> FromSqlResult<Self> {
        let mut as_str = value.as_str().unwrap();

        if ! (as_str.starts_with("[") && as_str.ends_with("]")) {
            Err(FromSqlError::InvalidType)
        } else {
            as_str = as_str.trim_matches(|char| char == '[' || char == ']'); // remove [ and ]
            Ok(GenreList::from(as_str.split(',').map(|elem| String::from(elem)).collect::<Vec::<String>>()))
        }
    }
}
impl ToSql for GenreList {
    fn to_sql(&self) -> rusqlite::Result<ToSqlOutput<'_>> {
        let mut as_string = String::new();

        as_string.push_str("[");
        for val in Into::<Vec::<String>>::into(self).iter() {
            as_string.push_str(val.clone().as_str());
            as_string.push_str(",");
        }
        as_string.push_str("]");

        Ok(ToSqlOutput::from(as_string.to_owned()))
    }
}

/***********\
|           |
|   ENUMS   |
|           |
\***********/

pub enum DataOrder {
    atoz,
    ztoa,
    oldtorecent,
    recenttoold,
    badtogood,
    goodtobad
}
impl DataOrder {
    pub fn as_string_for_movies(&self) -> String {
        match self {
            DataOrder::atoz => String::from("order by m.title asc"),
            DataOrder::ztoa => String::from("order by m.title desc"),
            DataOrder::oldtorecent => String::from("order by m.added_at asc"),
            DataOrder::recenttoold => String::from("order by m.added_at desc"),
            DataOrder::badtogood => String::from("order by m.imdb_rating asc"),
            DataOrder::goodtobad => String::from("order by m.imdb_rating desc")
        }
    }
    pub fn as_display_string(&self) -> String {
        match self {
            DataOrder::atoz => String::from("atoz"),
            DataOrder::ztoa => String::from("ztoa"),
            DataOrder::oldtorecent => String::from("oldtorecent"),
            DataOrder::recenttoold => String::from("recenttoold"),
            DataOrder::badtogood => String::from("badtogood"),
            DataOrder::goodtobad => String::from("goodtobad")
        }
    }
    pub fn from_str(string_slice: &str) -> Result<Self, String> {
        match string_slice {
            "atoz" => Ok(DataOrder::atoz),
            "ztoa" => Ok(DataOrder::ztoa),
            "oldtorecent" => Ok(DataOrder::oldtorecent),
            "recenttoold" => Ok(DataOrder::recenttoold),
            "badtogood" => Ok(DataOrder::badtogood),
            "goodtobad" => Ok(DataOrder::goodtobad),
            _ => Err(format!("Could not parse {} to DataOrder enum", string_slice))
        }
    }
    pub fn all_stringified_movie_values() -> Vec<String> {
        let mut result = Vec::with_capacity(6);

        result.push(DataOrder::atoz.as_display_string());
        result.push(DataOrder::ztoa.as_display_string());
        result.push(DataOrder::oldtorecent.as_display_string());
        result.push(DataOrder::recenttoold.as_display_string());
        result.push(DataOrder::badtogood.as_display_string());
        result.push(DataOrder::goodtobad.as_display_string());

        result
    }
}

/************\
|            |
|   STRUCTS  |
|            |
\************/

#[derive(Debug, Clone)]
pub struct Person {
    pub id:                 i32,
    pub name:               String,
    pub referenced:         u32,
    pub picture_location:   Option<String>,
    pub imdb_id:            String
}
impl Person {
    pub fn new() -> Person {
        Person {
            id: -1, name: String::new(), referenced: 0,
            picture_location: None, imdb_id: String::new()
        }
    }
}

/**********************************\
|                                  |
|   SMART INSERT/QUERY FUNCTIONS   |
|                                  |
\**********************************/

// return Person from database based on imdb_id
// if Person wasn't found, he will be inserted,
// the inserted person is then returned
pub async fn lookup_person(imdb_id: &String,
                           name: &String,
                           user_config: &crate::UserConfig,
                           query_stmt: &mut rusqlite::Statement<'_>,
                           update_stmt: &mut rusqlite::Statement<'_>,
                           insert_stmt: &mut rusqlite::Statement<'_>) -> Result<Person, String> {

    let client = RqClient::new();

    if let Some(mut person) = query_imdb_id_of_person(query_stmt, imdb_id) {
        // person exists in db
        // increment referenced count
        person.referenced += 1;
        increment_person_referenced_col(update_stmt, &person);

        Ok(person)

    } else {
        // person not yet present in db

        for api in &user_config.api_order {
            let key = user_config.get_api_key(api)?;
            let e_api =api_abstractions::Api::from(api);
            if let Ok(mut person) = api_abstractions::lookup_person_info(imdb_id, None, &e_api, &key, user_config).await {
                person.referenced += 1;

                insert_single_person(insert_stmt, &person);
                // to get the local db key, return result of query
                person = match query_imdb_id_of_person(query_stmt, imdb_id) {
                    Some(val) => val,
                    None => return Err(String::from("Could not find person that was just inserted"))
                };

                return Ok(person);
            }
        }

        // person was not found in any api

        let mut person = Person::new();
        person.name = name.clone();
        person.referenced += 1;
        person.imdb_id = imdb_id.clone();

        insert_single_person(insert_stmt, &person);
        person = match query_imdb_id_of_person(query_stmt, imdb_id) {
            Some(val) => val,
            None => return Err(String::from("Could not find person that was just inserted"))
        };

        Ok(person)

        //Err(format!("http request to IMDB returned with code {}", res.status().as_u16()))
    }
}

pub async fn lookup_person_uncached(imdb_id: &String, name: &String, user_config: &crate::UserConfig, db_name: &str) -> Result<Person, String> {
    let conn = rusqlite::Connection::open(db_name).unwrap();
    let mut person_query = prepare_query_imdb_id_of_person(&conn);
    let mut person_update = prepare_increment_reference_count(&conn);
    let mut person_insert = prepare_insert_person_stmt(&conn);
    lookup_person(imdb_id, name, user_config, &mut person_query, &mut person_update, &mut person_insert).await
}

/**************************\
|                          |
|   IMDB-API INTERACTION   |
|                          |
\**************************/

pub async fn download_picture(src: String, dst: String) -> Option<String> {
    let static_dst = format!("static/{}", dst);
    println!("downloading from src {} to dst {}", src, static_dst);
    let mut child = Command::new("wget")
                            .arg(src.clone())
                            .arg("-O")
                            .arg(static_dst.clone())
                            .spawn().expect("could not spawn cURL command");
    if !child.wait().expect("failed to wait on spawned cURL command").success() {
        // if we couldn't get the poster, it's not a big deal, just set it back to None
        None
    } else {
        Some(dst)
    }
}

/**********************\
|                      |
|   DATABASE QUERIES   |
|                      |
\**********************/

// query person based on imdb_id
// query preparation returns a named statement
pub fn prepare_query_imdb_id_of_person<'clf>(conn: &'clf rusqlite::Connection) -> rusqlite::Statement<'clf> {
    conn.prepare("select person_id, name, referenced, picture_location, imdb_id
                  from people where imdb_id = :imdb_id").unwrap()
}
pub fn query_imdb_id_of_person(stmt: &mut rusqlite::Statement, imdb_id: &String) -> Option<Person> {
    let mut rows = stmt.query_named(&[(":imdb_id", &imdb_id.as_str())]).unwrap();
    if let Some(result_row) = rows.next() {
        let row = result_row.unwrap();
        Some(Person {
            id: row.get(0),
            name: row.get(1),
            referenced: row.get(2),
            picture_location: row.get(3),
            imdb_id: row.get(4)
        })
    } else {
        None
    }
}
pub fn prepare_query_id_of_person<'clf>(conn: &'clf rusqlite::Connection) -> rusqlite::Statement<'clf> {
    conn.prepare("select person_id, name, referenced, picture_location, imdb_id
                  from people where person_id = :id").unwrap()
}
pub fn query_id_of_person(stmt: &mut rusqlite::Statement, id: i32) -> Result<Person, String> {
    let mut rows = stmt.query_named(&[(":id", &id)]).unwrap();
    if let Some(result_row) = rows.next() {
        let row = result_row.unwrap();
        Ok(Person {
            id: row.get(0),
            name: row.get(1),
            referenced: row.get(2),
            picture_location: row.get(3),
            imdb_id: row.get(4)
        })
    } else {
        Err(format!("person with id {} was not found", id))
    }
}

fn query_version(conn: &rusqlite::Connection, version: i32) -> Option<i32> {
    let mut stmt = conn.prepare("select * from versions where id = ?1").unwrap();
    let mut rows = stmt.query(&[&version]).unwrap();

    if let Some(row) = rows.next() {
        Some(row.unwrap().get(0))
    } else {
        None
    }
}

/***********************************\
|                                   |
|   DUMB DATABASE INSERTS/UPDATES   |
|                                   |
\***********************************/

// careful: this produces a named statement
//          the names are the same as the names of the members of the struct
pub fn prepare_insert_person_stmt<'clf>(conn: &'clf rusqlite::Connection) -> rusqlite::Statement<'clf> {
    conn.prepare("insert into people (name,
                                      referenced,
                                      picture_location,
                                      imdb_id)
                  values (:name,
                          :referenced,
                          :picture_location,
                          :imdb_id)")
        .unwrap()
}
pub fn insert_single_person(stmt: &mut rusqlite::Statement, person: &Person) {
    stmt.execute_named(&[(":name", &person.name.as_str()),
                         (":referenced", &person.referenced),
                         (":picture_location", &person.picture_location),
                         (":imdb_id", &person.imdb_id.as_str())]).unwrap();
}
pub fn prepare_increment_reference_count<'clf>(conn: &'clf rusqlite::Connection) -> rusqlite::Statement<'clf> {
    conn.prepare("update people set referenced = :referenced where person_id = :person_id")
        .unwrap()
}
pub fn increment_person_referenced_col(stmt: &mut rusqlite::Statement, person: &Person) {
    stmt.execute_named(&[(":referenced", &person.referenced),
                         (":person_id", &person.id)]).unwrap();
}

fn execute_sql_file(conn: &rusqlite::Connection, migration: &str) {
    // migration files can't be too big at the moment
    let contents = String::from_utf8(read(migration).unwrap()).unwrap();

    conn.execute_batch(contents.as_str()).unwrap();
}

/*******************\
|                   |
|   DATABASE INIT   |
|                   |
\*******************/

pub fn startup_db(db_path: &str, version: i32, migrations_dir: &str) {
    // check db existance
    if !Path::new(db_path).exists() {
        // db path does not exist: create new db from scratch
        init_db(db_path);
    }

    // db path exists: check version and migrate if needed
    let mut cur_v = version;
    let mut migrations_todo = Vec::new(); // use as stack
    let conn = rusqlite::Connection::open(db_path).unwrap();

    while cur_v > 0 {
        // build up stack of migrations
        // the sequence of which will result in converting current version into required
        // version
        if query_version(&conn, cur_v).is_some() {
            // reached local db version
            break; // start unwinding stack of migrations
        }
        migrations_todo.push(format!("{}/{}to{}.sql", migrations_dir, (cur_v-1), cur_v));
        cur_v -= 1;
    }
    while let Some(migration) = migrations_todo.pop() {
        execute_sql_file(&conn, migration.as_str()); // execute each migration in order
    }
}

fn init_db(db_path: &str){
    let conn = rusqlite::Connection::open(db_path).unwrap();

    conn.execute(
        "
        pragma foreign_keys = ON;
        ",
        &[]
    ).unwrap();

    // now add tables if they don't yet exist
    // table 1: people
    conn.execute(
        "
        create table if not exists people (
            person_id           integer primary key,
            name                text,
            referenced          integer,
            picture_location    text,
            imdb_id             text not null
        )
        ",
        &[]
    ).unwrap();
    // table 2: movies
    conn.execute(
        "
        create table if not exists movies (
            movie_id        integer primary key,
            title           text not null,
            description     text not null,
            director_id     integer,
            genres          text,
            release_year    integer not null,
            mins_duration   integer not null,
            added_at        text not null,
            imdb_rating     text,
            movie_location  text not null,
            poster_location text,
            imdb_id         text not null,
            complete        integer,
            foreign key(director_id) references people(person_id)
        )
        ",
        &[]
    ).unwrap();
    // table 3: cast and crew
    //          for many-to-many relation between movies and people
    //          added for every element in every field of Movie where there's a Vec<Box<Person>>
    //              type: 1 -> actor
    //                    2 -> writer
    //                    3 -> cinematography
    //                    4 -> music
    //          if type == 1
    //              role != null
    //                   = character portrayed by the actor
    conn.execute(
        "
        create table if not exists cast_and_crew (
            cnc_id              integer primary key,
            type                integer,
            role                text,
            movie_id            integer,
            person_id           integer,
            foreign key(movie_id) references movies(movie_id),
            foreign key(person_id) references people(person_id)
        )
        ",
        &[]
    ).unwrap();
    // table 4: versions
    //          for further changes, we'll keep track of version numbers
    //          allowing to migrate to new versions
    conn.execute(
        "
        create table if not exists versions (
            id                  integer primary key,
            project_version     text not null
        )
        ",
        &[]
    ).unwrap();

    // no close needed on conn because it's going out of scope
}
