#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;

use theatron;

fn main() {
    theatron::rocket_init().launch();
}
