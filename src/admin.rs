use time::Duration;
use std::time::Duration as stdDuration; // seriously?! how is this allowes?
use std::sync::RwLock;
use rocket::http::{Cookie, Cookies, SameSite};
use rocket::response::{Redirect, Flash, status::NoContent};
use rocket::request::{Form, FlashMessage};
use rocket_contrib::templates::Template;
use rocket_contrib::json::Json;
use tera::Context;

use crate::scrubber::common as ScrubberCommon;
use crate::Logger;
use crate::RocketConfig;
use crate::UserConfig;

// struct for handling state of accessible directories
pub mod datadirs {
    // separate mod for namespace: new is too generic a name
    use std::path::Path;
    use std::ffi::OsStr;

    pub struct DataDirs {
        pub accessible:     Vec<String>,
        pub image_dir:      String,
        pub video_dest:     String
    }
    fn check_existance<S: AsRef<OsStr> + ?Sized>(path_dir: &S) -> bool {
        let path = Path::new(path_dir);
        path.exists() && path.is_dir()
    }
    pub fn new(cfg_dirs:    Vec<String>,
               _image_dir:  &str,
               _video_dest: &str
    ) -> DataDirs {
        let mut image_dir = String::new();
        if check_existance(_image_dir) {
            image_dir = String::from(_image_dir);
        }
        let mut video_dest = String::new();
        if check_existance(_video_dest) {
            video_dest = String::from(_video_dest);
        }

        let __accessible = cfg_dirs.iter()
                                   .filter(|dir| check_existance(dir.as_str()))
                                   .map(|val| val.clone())
                                   .collect();

        DataDirs {
            accessible:     __accessible,
            image_dir:      image_dir,
            video_dest:     video_dest
        }
    }
}

#[derive(FromForm)]
pub struct ScrubRequest {
    directory: String,
    movies: Option<String>,
    series: Option<String>,
    include_srt: Option<String>,
}

#[get("/admin")]
pub fn admin_index(mut cookies: Cookies) -> Redirect {
    cookies.add(Cookie::build("lastTab", "admin").max_age(Duration::weeks(50)).same_site(SameSite::Strict).finish());
    Redirect::to(String::from("/admin/db_mgmt"))
}

#[get("/db_mgmt")]
pub fn db_mgmt(flash: Option<FlashMessage>, dirs: rocket::State<datadirs::DataDirs>) -> Template {
    let mut ctx = Context::new();
    ctx.insert("scrub_dirs", &dirs.accessible);
    if let Some(msg) = flash {
        println!("flash detected");
        ctx.insert("flash", &msg.msg());
    }

    Template::render("admin/db_mgmt", &ctx)
}

#[post("/db_mgmt/scrub_request", data = "<req>")]
pub fn scrub_request(req: Form<ScrubRequest>,
                     logger_rw: rocket::State<RwLock<Logger>>,
                     rocketConfig: rocket::State<RwLock<RocketConfig>>,
                     scrubber_rw: rocket::State<RwLock<ScrubberCommon::Scrubber>>
                    ) -> Flash<Redirect>
{
    let config = rocketConfig.read().unwrap();

    if config.env_is_dev {
        let mut logger = logger_rw.write().unwrap(); // lock ok
        logger.log("scrub request received");
    }

    let scrubber_r = scrubber_rw.read().unwrap();
    if scrubber_r.check_status() != ScrubberCommon::Status::Inactive {
        // scrubber is active: can't start it at the moment
        //      -> scrub_request failed

        if config.env_is_dev {
            let mut logger = logger_rw.write().unwrap();
            logger.log("scrubber was active during request");
        }

        Flash::error(Redirect::to("/admin/db_mgmt"), "Scrubber was active already: request failed")
    } else {
        // from here on we know that the scrubber thread is available
        //      -> set scrubber config and start it
        drop(scrubber_r); // drop read value so that write lock can be taken
        let mut scrubber_w = scrubber_rw.write().unwrap();

        scrubber_w.set_config(req.directory.clone(),
                              req.movies.is_some(),
                              req.series.is_some(),
                              req.include_srt.is_some());
        if config.env_is_dev {
            let mut logger = logger_rw.write().unwrap();
            logger.log(format!("started scrub with options: {}, movies: {}, series: {}, include_srt: {}",
                               req.directory,
                               req.movies.is_some(),
                               req.series.is_some(),
                               req.include_srt.is_some()).as_str());
        }
        scrubber_w.start(config.db_path.clone());

        Flash::success(Redirect::to("/admin/status"), "started scrub...")
    }
}

#[get("/status")]
pub fn status(flash: Option<FlashMessage>) -> Template {
    let mut ctx = Context::new();
    if let Some(msg) = flash {
        println!("flash detected");
        ctx.insert("flash", &msg.msg());
    }
    Template::render("admin/status", &ctx)
}

#[get("/scrub_status_json")]
pub fn scrub_status(scrubber_rw: rocket::State<RwLock<ScrubberCommon::Scrubber>>) -> Json<ScrubberCommon::ScrubberStats> {
    let scrubber_r = scrubber_rw.read().unwrap();
    let stat = scrubber_r.get_serializable_status();
    Json(stat)
}

#[get("/scrub_status_update")]
pub fn scrub_status_update(scrubber_rw: rocket::State<RwLock<ScrubberCommon::Scrubber>>) -> Result<Json<ScrubberCommon::ScrubberStats>, NoContent> {
    let user_config = UserConfig::parse_config_fallback();
    let mut scrubber = scrubber_rw.write().unwrap();
    let mut rx = scrubber.subscribe_to_update();
    drop(scrubber);
    let res = rx.recv_timeout(stdDuration::from_secs(user_config.update_timeout_secs));
    if res.is_ok() {
        let scrubber_r = scrubber_rw.read().unwrap();
        let stat = scrubber_r.get_serializable_status();
        Ok(Json(stat))
    } else {
        Err(NoContent)
    }
}
