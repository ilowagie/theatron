#[macro_export]
macro_rules! signal_on_bus {
    ($bus:expr) => {
        let mut bus = $bus.lock().unwrap();
        if bus.try_broadcast(true) == Err(true) {
            // bus is probably full, empty it
            let mut rx = bus.add_rx();
            for i in 1..10 {
                rx.recv();
            }
            bus.broadcast(true);
        }
        drop(bus);
    }
}

#[macro_export]
macro_rules! increment_mutex_option {
    ($mutex:expr, $name:ident) => {
        let mut $name = $mutex.lock().unwrap();
        (*$name) = match *$name {
            None => Some(1),
            Some(val) => Some(val + 1)
        };
        drop($name); // often unnecessary, just to be sure
    }
}

#[macro_export]
macro_rules! set_to_val {
    ($mutex:expr, $val:expr) => {
        let mut tmp = $mutex.lock().unwrap();
        (*tmp) = $val;
        drop(tmp);
    }
}
