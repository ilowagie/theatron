use std::process::{Command, Child};
use std::convert::TryFrom;

// use ffprobe to extract video codec
pub fn get_video_codec(file_path: &String) -> Result<String, String> {
    let command_output = Command::new("ffprobe").arg("-v").arg("error")
                                                .arg("-select_streams").arg("v:0")
                                                .arg("-show_entries").arg("stream=codec_name")
                                                .arg("-of").arg("default=noprint_wrappers=1:nokey=1")
                                                .arg(file_path)
                                                .output();
    if let Ok(out) = command_output {
        let stdout = String::from_utf8_lossy(&out.stdout);
        if out.status.success() {
            Ok(stdout.to_owned().to_string().trim().to_string())
        } else {
            Err(format!("errors encountered while using ffprobe {}", String::from_utf8_lossy(&out.stderr)))
        }
    } else {
        Err(String::from("could not spawn ffprobe command"))
    }
}

// returns Ok(true) if mp4 file has faststart flag enabled (moov metadata before mdat)
pub fn check_for_mp4_faststart(file: &String) -> Result<bool, String> {
    Command::new("./helper_scripts/check_faststart.sh").arg(file.as_str())
        .status().map_err(|e| format!("{}", e)).map(|status| status.success())
}

pub fn copy_video_to_faststart_mp4(from: &String, to: &String) -> Result<Child, String> {
    Command::new("ffmpeg").args(&["-i", from.as_str(), "-c:v", "copy", "-c:a", "copy",
                                  "-movflags", "+faststart", to.as_str()])
                          .spawn()
                          .map_err(|e| format!("{}", e))
}

pub fn convert_with_ffmpeg(from: &String, to: &String) -> Result<Child, String> {
    let user_config = crate::UserConfig::parse_config_fallback();
    if user_config.ffmpeg_enable_vaapi {
        Command::new("ffmpeg").args(&["-vaapi_device", "/dev/dri/renderD128", "-i", from.as_str(),
                                      "-vf", "format=nv12,hwupload", "-c:v", "h264_vaapi", "-q", "0", "-rc_mode", "CQP",
                                      "-c:a", "copy", "-movflags", "+faststart", to.as_str()])
                              .spawn()
                              .map_err(|e| format!("{}", e))
    } else {
        Command::new("ffmpeg").args(&["-threads", "8",
                                      "-c:v", "libx264", "-crf", "0", "-preset", "ultrafast",
                                      "-c:a", "copy", "-movflags", "+faststart", to.as_str()])
                              .spawn()
                              .map_err(|e| format!("{}", e))
    }
}

pub fn convert_srt_to_vtt(from: &String, to: &String) -> Result<(), String> {
    let status = Command::new("ffmpeg").arg("-i").arg(from)
                                       .arg(to).status().unwrap();
    if !status.success() {
        Err(format!("failed to convert {} to {}", from, to))
    } else {
        Ok(())
    }
}

pub fn extract_sub_track_n_from_container(video_container_file: &String, n: i32, destination: &String) -> Result<(), String> {
    let status = Command::new("ffmpeg").arg("-i").arg(video_container_file)
                                       .arg("-map").arg(format!("0:s:{}", n))
                                       .arg("-c:s").arg("copy").arg(destination)
                                       .status().unwrap();
    if !status.success() {
        Err(format!("failed to extract subtitle track {} from {} to {}", n, video_container_file, destination))
    } else {
        Ok(())
    }
}

pub fn extract_lang_subtitle_track_from_container(video_container_file: &String, lang: &String, destination: &String) -> Result<(), String> {
    let status = Command::new("ffmpeg").arg("-i").arg(video_container_file)
                                       .arg("-map").arg(format!("0:m:language:{}", lang))
                                       .arg("-c:s").arg("copy").arg(destination)
                                       .status().unwrap();
    if !status.success() {
        Err(format!("failed to extract subtitle track in language {} from {} to {}", lang, video_container_file, destination))
    } else {
        Ok(())
    }
}

pub fn get_subtitle_tracks(video_container_file: &String) -> Result<Vec<String>, String> {
    let output = Command::new("ffprobe").arg("-loglevel").arg("error")
                                        .arg("-select_streams").arg("s")
                                        .arg("-show_entries").arg("stream=index:stream_tags=language")
                                        .arg("-of").arg("csv=p=0").arg(video_container_file)
                                        .output();
    if let Ok(out) = output {
        if out.status.success() {
            let stdout = String::from_utf8_lossy(&out.stdout);
            let mut lang_vec = Vec::new();
            let mut split_out = stdout.split(',');
            while split_out.next().is_some() {
                // the splitted result is expected to be in pairs
                // first iterator element is an index (which we ignore),
                // second one is the language, which is pushed in the lang_vec
                lang_vec.push(String::from(split_out.next().unwrap_or("")));
            }
            Ok(lang_vec)
        } else {
            println!("an untested function received an error, would you kindly report in the gitlab issue tracker? Thank you! :)");
            let e = format!("failed to get subtitle tracks information for {}: {}", video_container_file, String::from_utf8_lossy(&out.stderr));
            println!("error: {}", e);
            Err(e)
        }
    } else {
        println!("an untested function received an error, would you kindly report in the gitlab issue tracker? Thank you! :)");
        Err(format!("failed to get subtitle tracks information for {}", video_container_file))
    }
}

pub fn copy(src: &String, dst: &String) -> Result<(), String> {
    // using cp command, best way to assure CoW on CoW filesystems
    // since we handle high-definition movies this has a huge impact on disk usage
        let status = Command::new("cp").arg("--reflink=auto")
                                       .arg(src)
                                       .arg(dst)
                                       .status().unwrap();
        if !status.success() {
            return Err(format!("could not copy {} to {}", src, dst))
        }
    Ok(())
}

pub fn make_parent_dirs(path_string: &String) -> Result<(), String> {
    let status = Command::new("mkdir").arg("-p").arg(path_string)
                                      .status().unwrap();
    if !status.success() {
        Err(format!("could not create path {}", path_string))
    } else {
        Ok(())
    }
}
