use super::video_helpers as vhelp;

use std::sync::{Arc, Mutex};
use std::fs;
use std::io;
use std::path::Path;
use std::process::{Command, Child};
use std::convert::TryInto;
use regex::Regex;
use bus::{Bus, BusReader};

use crate::datahandling::movies as moviehandling;

pub async fn scrub_movies(add_srt: bool, entry_result: io::Result<fs::DirEntry>,
                      done_mutex_arc: Arc<Mutex<Option<u32>>>,
                      failed_mutex_arc: Arc<Mutex<Option<u32>>>,
                      errors_mutex_arc: Arc<Mutex<Vec<String>>>,
                      transcoding_amount_arc: Arc<Mutex<Option<u32>>>,
                      database_path: &String,
                      user_config: &crate::UserConfig,
                      broadcast_mtx: Arc<Mutex<Bus<bool>>>)
    -> Option<Child>
{
    let mut child_opt = None;
    if let Ok(entry) = entry_result {
        let res = scrub_entry(entry, database_path, user_config, add_srt).await;
        if let Err(e) = res {
            crate::increment_mutex_option!(failed_mutex_arc, failed_amount);
            let mut errors = errors_mutex_arc.lock().unwrap();
            (*errors).push(e);
        } else {
            child_opt = res.unwrap();
            if child_opt.is_some() {
                crate::increment_mutex_option!(transcoding_amount_arc, transcoding_amount);
            }
        }
    }
    // done amount increments for every entry, even when there were errors
    // so it can be used with entry_amount to draw some sort of progress bar
    crate::increment_mutex_option!(done_mutex_arc, done_amount);

    crate::signal_on_bus!(broadcast_mtx);

    child_opt
}

async fn scrub_entry(entry: fs::DirEntry, database_path: &String, user_config: &crate::UserConfig, add_srt: bool) -> Result<Option<Child>, String> {
    let path = entry.path();
    let path = path.as_path();
    let mut in_dir = false;

    let name = String::from(path.file_name().unwrap().to_str().unwrap());

    let (movie_file_path, file_extension) = if path.is_dir() {
        in_dir = true;
        find_movie_file(path).await?
    } else {
        (String::from(path.as_os_str().to_str().unwrap()), String::from(path.extension().unwrap().to_str().unwrap()))
    };
    let cli_movie_file_path = movie_file_path.replace(' ', "\\ ");

    let (title, year) = guess_title_year(&name).await?;

    let mut child_opt = None;
    let mut destination = String::from("static/");
    // check container
    if let Some(supported_codecs) = user_config.allowed_video_formats.get(&file_extension) {
        destination.push_str(form_movie_destination(&title, &year, user_config, &file_extension, &user_config.title_word_delim).as_str());

        if !Path::new(destination.as_str()).exists() {
            // check codec
            let codec = vhelp::get_video_codec(&movie_file_path)?;
            if supported_codecs.contains(&codec) {
                if vhelp::check_for_mp4_faststart(&cli_movie_file_path)? {
                    vhelp::copy(&movie_file_path, &destination)?;
                } else {
                    // move moov to front of file
                    child_opt = Some(vhelp::copy_video_to_faststart_mp4(&movie_file_path, &destination)?);
                }
            } else {
                let new_extension = String::from("mp4");
                destination = format!("static/{}", form_movie_destination(&title, &year, user_config, &new_extension, &user_config.title_word_delim));
                if !Path::new(destination.as_str()).exists() {
                    child_opt = Some(vhelp::convert_with_ffmpeg(&movie_file_path, &destination)?);
                }
            }
        }
    } else {
        // NOTE maybe this can be optimized where if a supported codec is used in an unsupported
        // container a different ffmpeg command is issued where -c:v copy is used
        let new_extension = String::from("mp4");
        destination.push_str(form_movie_destination(&title, &year, user_config, &new_extension, &user_config.title_word_delim).as_str());
        if !Path::new(destination.as_str()).exists() {
            let supported_codecs = user_config.allowed_video_formats.get("mp4").unwrap();
            let codec = vhelp::get_video_codec(&movie_file_path)?;
            child_opt = if supported_codecs.contains(&codec) {
                Some(vhelp::copy_video_to_faststart_mp4(&cli_movie_file_path, &destination)?)
            } else {
                Some(vhelp::convert_with_ffmpeg(&movie_file_path, &destination)?)
            };
        }
    }


    let movie = moviehandling::smart_lookup_movie(title, year, database_path.as_str(), &destination, user_config).await?;

    if add_srt {
        if in_dir {
            let dir_path = entry.path().to_string_lossy().into_owned();
            scrub_dir_for_movie_subs(&dir_path, movie.id, database_path.as_str(), user_config);
        }
        scrub_container_for_movie_subs(&movie_file_path, movie.id, database_path.as_str(), user_config);
    }

    Ok(child_opt)
}


// returns a Result where Ok() contains a tuple of strings
// the first value is the movie's file path
// the second value is the movie's file extension
pub async fn find_movie_file(path: &Path) -> Result<(String, String), String> {

    let mut num_movie_files: u32 = 0;
    let mut files = Vec::new();

    let entries = simple_error_handling!(fs::read_dir(path), format!("could not open path {:?}", path));
    for entry in entries.filter(|res| res.is_ok()).map(|res| res.unwrap()) {
        let file_cmd = Command::new("file").arg("-b")
                                           .arg("--mime-type")
                                           .arg(entry.path().as_path().as_os_str().to_str().unwrap())
                                           .output();
        if let Ok(out) = file_cmd {
            let stdout = String::from_utf8_lossy(&out.stdout);
            if out.status.success() && stdout.starts_with("video/") {
                let path = entry.path();
                files.push(path.as_path().to_owned());
                num_movie_files += 1;
            }
        }
    }

    if num_movie_files == 0 {
        Err(format!("no movie files found in {:?}", path))
    } else if num_movie_files == 1 {
        let movie_path = String::from(files[0].as_os_str().to_str().unwrap());
        let extension = String::from(files[0].extension().unwrap().to_str().unwrap());
        Ok((movie_path, extension))
    } else {
        // when there are multiple video files, find out if they're
        //      +/- same length -> series directory -> skip because we're only interested in movies
        //      completely different length -> probably some trailers/sample -> biggest one will
        //      probably be a movie
        let mut longest_is_double = false;
        let mut longest_duration = 0_f32;
        let mut longest_path = None;
        // NOTE important: what is the expected max difference in length of series episodes?
        // should maybe be a percentage?
        let threshold = 0.1_f32;
        for file in files.iter() {
            let duration_command = Command::new("ffprobe").arg("-v").arg("error")
                                                          .arg("-show_entries").arg("format=duration")
                                                          .arg("-of").arg("default=noprint_wrappers=1:nokey=1")
                                                          .arg(file.as_os_str())
                                                          .output();
            if let Ok(out) = duration_command {
                let stdout = String::from_utf8_lossy(&out.stdout);
                let duration: f32 = stdout.trim().parse().map_err(|_| String::from("could not parse duration of movie"))?;
                if (longest_duration - duration).abs() < longest_duration*threshold {
                    // We have found a movie file of +/- equal length.
                    // Both files could be trailers/samples OR they could be episodes from a series
                    longest_is_double = true;
                } else if duration > longest_duration {
                    longest_duration = duration;
                    longest_is_double = false;
                    longest_path = Some(file);
                }
            }
        }
        if longest_is_double {
            Err(String::from("find two files that are about the same in length: this might be a series"))
        } else if let Some(val) = longest_path {
            let movie_path = String::from(val.as_os_str().to_str().unwrap());
            let extension = String::from(val.extension().unwrap().to_str().unwrap());
            Ok((movie_path, extension))
        } else {
            unreachable!()
        }
    }

}

pub async fn guess_title_year(name: &String) -> Result<(String, String), String> {
    lazy_static! {
        static ref MOVIE_YEAR_RE: Regex = Regex::new(r"^(?P<title>([a-zA-Z0-9']+[\.\- _]?)*?[a-zA-Z]+)[\.\- _]?\[?\(?\{?(?P<year>[0-9]{4})\}?\)?\]?").unwrap();
    }

    let captures = match MOVIE_YEAR_RE.captures(name.as_str()) {
        None => return Err(format!("failed to guess title for file {}, nothing seems to match the Regex", name)),
        Some(val) => val
    };
    let title_match = match captures.name("title") {
        None => return Err(format!("failed to guess title for file {}, nothing seems to match the Regex", name)),
        Some(val) => String::from(val.as_str())
    };
    let title_parts = title_match.split(&['.', ',', '-', '_'][..]);
    let mut title = String::new();
    for part in title_parts {
        title.push_str(part);
        title.push(' ');
    }
    title.pop(); // remove trailing space

    let year = match captures.name("year") {
        None => String::new(),
        Some(val) => String::from(val.as_str())
    };

    Ok((title, year))
}

fn form_movie_destination(title: &String, year: &String, user_config: &crate::UserConfig, file_extension: &String, title_word_delim: &String) -> String {
    let mut result = String::new();
    result.push_str(user_config.video_dest.as_str());
    if !result.ends_with('/') {
        result.push_str("/");
    }

    // make titles nicer to handle in commandline
    // TODO: formatting of filenames as config option
    let title_parts = title.split(' ');
    for part in title_parts {
        result.push_str(part);
        result.push_str(title_word_delim.as_str());
    }

    if year.len() > 1 {
        result.push_str(year.as_str());
    } else {
        result.pop();
    }

    result.push('.');
    result.push_str(file_extension.as_str());

    result
}

fn is_subtitle_file(path_string: &String) -> bool {
    let mime = crate::get_mime_type_string(path_string.as_str());
    let correct_mime = mime.is_ok() && (mime.unwrap() == String::from("text/plain"));

    let ext = path_string.rsplit('.').next().unwrap_or("");
    let correct_ext =  ext == "srt" || ext == "vtt";

    correct_mime && correct_ext
}

fn is_srt_file(path_string: &String) -> bool {
    let ext = path_string.rsplit('.').next().unwrap_or("");
    ext == "srt"
}

fn get_subtitle_lang(path_string: &String) -> Option<String> {
    // Note: lots of assumptions here for the moment
    //  assumption 1: languages are always 3 letters and separated with dots from rest of filename
    //  assumption 2: if language pattern does not match (but name is still valid) then fall back
    //                to english as a lot of movies don't provide multisub, but just english subs in a file with
    //                same name as movie
    // TODO: get more multisub examples to check validity of assumptions
    path_string.rsplit('.').nth(1).map_or(None, |val| if val.len() == 3 { Some(val.to_string()) } else { Some(String::from("eng")) })
}

// if movie is in a directory, look for *.srt or *.vtt
fn scrub_dir_for_movie_subs(dir: &String, movie_id: i32, db_path: &str, user_config: &crate::UserConfig) {
    let path = Path::new(dir);

    if let Ok(dir_entries) = fs::read_dir(path) {
        for dir_entry in dir_entries {
            if let Ok(entry) = dir_entry {
                if entry.path().is_dir() {
                    let new_dir = entry.path().to_string_lossy().into_owned();
                    scrub_dir_for_movie_subs(&new_dir, movie_id, db_path, user_config);
                } else {
                    let entry_path_string = entry.path().to_string_lossy().into_owned();
                    if is_subtitle_file(&entry_path_string) {
                        let mut movie_subtitle = None;
                        let lang_guess = get_subtitle_lang(&entry_path_string);

                        if lang_guess.is_some() && user_config.subtitle_langs.len() > 0 {
                            let lang = lang_guess.unwrap();
                            if user_config.subtitle_langs.contains(&lang) {
                                movie_subtitle = Some(moviehandling::MovieSubtitle {
                                    id: 0, // temp before insert
                                    location: format!("{}/{}/{}.vtt", user_config.subtitle_dir, movie_id, lang),
                                    language: lang
                                });
                            }
                        } else if lang_guess.is_some() {
                            let lang = lang_guess.unwrap();
                            movie_subtitle = Some(moviehandling::MovieSubtitle {
                                id: 0, // temp before db insert
                                location: format!("{}/{}/{}.vtt", user_config.subtitle_dir, movie_id, lang),
                                language: lang
                            });
                        }
                        if let Some(sub) = movie_subtitle {
                            let dst_dir = format!("static/{}/{}", user_config.subtitle_dir, movie_id);
                            vhelp::make_parent_dirs(&dst_dir);
                            let real_dst = format!("static/{}", sub.location);

                            if !Path::new(&real_dst).exists() {
                                // check extension, convert if needed:
                                if is_srt_file(&entry_path_string) {
                                    vhelp::convert_srt_to_vtt(&entry_path_string, &real_dst);
                                } else {
                                    vhelp::copy(&entry_path_string, &real_dst);
                                }

                                moviehandling::add_subtitle(db_path, movie_id, sub);
                            }
                        }
                    }
                }
            }
        }
    }
}

// check movie container for subtitle tracks, extract them if needed
fn scrub_container_for_movie_subs(container: &String, movie_id: i32, db_path: &str, user_config: &crate::UserConfig) {
    let lang_vec = vhelp::get_subtitle_tracks(container).unwrap_or(Vec::new());
    for lang in lang_vec {
        if user_config.subtitle_langs.len() == 0 || user_config.subtitle_langs.contains(&lang) {
            let sub = moviehandling::MovieSubtitle {
                id: 0, // temp before db insert
                location: format!("{}/{}/{}.vtt", user_config.subtitle_dir, movie_id, lang),
                language: lang.clone()
            };

            let dst_dir = format!("static/{}/{}", user_config.subtitle_dir, movie_id);
            vhelp::make_parent_dirs(&dst_dir);
            let real_dst = format!("static/{}", sub.location);

            if !Path::new(&real_dst).exists() {
                vhelp::extract_lang_subtitle_track_from_container(container, &lang, &real_dst);
                moviehandling::add_subtitle(db_path, movie_id, sub);
            }
        }
    }
}
