use super::movies;

use std::thread;
use std::time::Duration;
use std::mem;
use std::fs;
use std::io;
use std::path::Path;
use std::sync::{Arc, Mutex};
use std::boxed::Box;
use std::process::{Command, Child};
use std::convert::TryInto;

use serde::{Serialize, Deserialize};
use bus::{Bus, BusReader};
use futures::stream::{self, StreamExt};

#[derive(PartialEq, Debug, Clone, Copy)]
pub enum Status {
    Inactive,
    Working,
    Done
}

#[derive(Serialize, Deserialize)]
pub struct ScrubberStats {
    pub status:             String, // Status enum is converted to string
    pub entry_amount:       Option<u32>,
    pub done_amount:        Option<u32>,
    pub transcoding_amount: Option<u32>,
    pub transcoding_done:   Option<u32>,
    pub failed_amount:      Option<u32>,
    pub errors:             Vec<String>,
    pub incl_movies:        bool,
    pub incl_series:        bool,
    pub add_srt:            bool
} // struct for updates to client on scrubber's current status in JSON

pub struct Scrubber {
    // thread context:
    handle:             Option<thread::JoinHandle<()>>,
    bus:                Arc<Mutex<Bus<bool>>>,
    async_rt:           Arc<Mutex<tokio::runtime::Runtime>>,
    // settings:
    scrub_dir:          String,
    incl_movies:        bool,
    incl_series:        bool,
    add_srt:            bool,
    // status:
    status:             Arc<Mutex<Status>>,
    entry_amount:       Arc<Mutex<Option<u32>>>,
    done_amount:        Arc<Mutex<Option<u32>>>,
    transcoding_amount: Arc<Mutex<Option<u32>>>,
    transcoding_done:   Arc<Mutex<Option<u32>>>,
    failed_amount:      Arc<Mutex<Option<u32>>>,
    errors:             Arc<Mutex<Vec<String>>>,
    status_change_rx:   BusReader<bool>
}

impl Clone for Scrubber {
    fn clone(&self) -> Self {
        let mut bus = self.bus.lock().unwrap();
        let rx = bus.add_rx();
        drop(bus);
        Scrubber {
            handle: None,
            bus: Arc::clone(&self.bus),
            async_rt: Arc::clone(&self.async_rt),
            scrub_dir: self.scrub_dir.clone(),
            incl_movies: self.incl_movies.clone(),
            incl_series: self.incl_series.clone(),
            add_srt: self.add_srt.clone(),
            status: Arc::clone(&self.status),
            entry_amount: Arc::clone(&self.entry_amount),
            done_amount: Arc::clone(&self.done_amount),
            transcoding_amount: Arc::clone(&self.transcoding_amount),
            transcoding_done: Arc::clone(&self.transcoding_done),
            failed_amount: Arc::clone(&self.failed_amount),
            errors: Arc::clone(&self.errors),
            status_change_rx: rx
        }
    }
}

impl Scrubber {
    pub fn new() -> Scrubber {
        let mut bus = Bus::new(20);
        let rx = bus.add_rx();
        Scrubber {
            handle: None,
            bus: Arc::new(Mutex::new(bus)),
            async_rt: Arc::new(Mutex::new(tokio::runtime::Runtime::new().unwrap())),
            scrub_dir: String::from(""),
            incl_movies: false,
            incl_series: false,
            add_srt: false,
            status: Arc::new(Mutex::new(Status::Inactive)),
            entry_amount: Arc::new(Mutex::new(None)),
            done_amount: Arc::new(Mutex::new(None)),
            transcoding_amount: Arc::new(Mutex::new(None)),
            transcoding_done: Arc::new(Mutex::new(None)),
            failed_amount: Arc::new(Mutex::new(None)),
            errors: Arc::new(Mutex::new(Vec::new())),
            status_change_rx: rx
        }
    }
    pub fn set_config(&mut self, scrub_dir: String, incl_movies: bool, incl_series: bool, add_srt: bool) {
        self.scrub_dir = scrub_dir;
        self.incl_movies = incl_movies;
        self.incl_series = incl_series;
        self.add_srt = add_srt;
    }
    pub fn start(&mut self, db_path: String) {
        let mut status = self.status.lock().unwrap();
        *status = Status::Working;
        drop(status);

        let scrub_clone = self.clone();
        let handle = thread::spawn(move || {
            let rt_arc = scrub_clone.async_rt.clone();
            let mut rt = rt_arc.lock().unwrap();
            (*rt).block_on(scrub_clone.scrub(db_path));
        });
        self.handle = Some(handle);

        crate::signal_on_bus!(self.bus);
    }
    async fn scrub(&self, db_path: String) {
        let path = Path::new(&self.scrub_dir);
        if let Ok(dir_entries) = fs::read_dir(path) {

            // start with fresh scrubber values
            let count = fs::read_dir(path).unwrap().count();
            crate::set_to_val!(self.entry_amount, Some(count.try_into().unwrap()));
            crate::set_to_val!(self.done_amount, Some(0));
            crate::set_to_val!(self.failed_amount, Some(0));
            crate::set_to_val!(self.transcoding_amount, Some(0));
            crate::set_to_val!(self.transcoding_done, Some(0));
            let mut errors = self.errors.lock().unwrap();
            (*errors).clear();
            drop(errors);

            // signal fresh values and start of scrub
            crate::signal_on_bus!(self.bus);

            // get config values for video_dest and image_dir
            let user_config_location = String::from("config/theatron.json");
            let user_config = crate::UserConfig::parse_config_fallback();

            if self.incl_movies {
                let mut futures = Vec::new();
                for entry in dir_entries {
                    futures.push(movies::scrub_movies(self.add_srt, entry, Arc::clone(&self.done_amount),
                                                             Arc::clone(&self.failed_amount), Arc::clone(&self.errors),
                                                             Arc::clone(&self.transcoding_amount),
                                                             &db_path, &user_config,
                                                             Arc::clone(&self.bus)));
                }
                let mut children = Vec::new();
                for future in futures {
                    if let Some(child) = future.await {
                        children.push(child);
                    }
                }
                for mut child in children {
                    let ecode = child.wait().unwrap();
                    if !ecode.success() {
                        // limit lock time for mutex
                        let mut errors = self.errors.lock().unwrap();
                        (*errors).push(format!("transcoding job returned non-zero exit code {:?}", ecode));
                        drop(errors);
                    }
                    crate::increment_mutex_option!(self.transcoding_done, transcoding_done);
                    crate::signal_on_bus!(self.bus);
                }
            }
            if self.incl_series {
                // TODO further implement scrubbing
            }
        }
        let mut status = self.status.lock().unwrap();
        *status = Status::Done;
        drop(status);

        crate::signal_on_bus!(self.bus);
    }
    pub fn check_status(&self) -> Status {
        let mut status = self.status.lock().unwrap();
        if *status == Status::Done {
            *status = Status::Inactive;
            crate::signal_on_bus!(self.bus);
            return Status::Done;
        }
        return *status;
    }
    pub fn status_as_string(&self) -> String {
        match self.check_status() {
            Status::Done => String::from("done"),
            Status::Inactive => String::from("inactive"),
            Status::Working => String::from("working")
        }
    }
    pub fn get_serializable_status(&self) -> ScrubberStats {
        let status = self.status_as_string();
        let entry_amount = self.entry_amount.lock().unwrap();
        let done_amount = self.done_amount.lock().unwrap();
        let transcoding_amount = self.transcoding_amount.lock().unwrap();
        let transcoding_done = self.transcoding_done.lock().unwrap();
        let failed_amount = self.failed_amount.lock().unwrap();
        let errors = self.errors.lock().unwrap();
        ScrubberStats {
            status:             status,
            entry_amount:       *entry_amount,
            done_amount:        *done_amount,
            transcoding_amount: *transcoding_amount,
            transcoding_done:   *transcoding_done,
            failed_amount:      *failed_amount,
            errors:             (*errors).clone(),
            incl_movies:        self.incl_movies,
            incl_series:        self.incl_series,
            add_srt:            self.add_srt
        }
    }
    pub fn subscribe_to_update(&mut self) -> BusReader<bool> {
        let mut bus = self.bus.lock().unwrap();
        bus.add_rx()
    }
    pub fn wait_on_complete(&mut self) {
        while {
            // while condition, implicit returns are sometimes confusing
            let status = self.status.lock().unwrap();
            *status == Status::Working
        } {
            // while body
            self.status_change_rx.recv();
        }
        // reaquire status lock for changin
        let mut status = self.status.lock().unwrap();
        *status = Status::Inactive;
        let mut handle = None;
        mem::swap(&mut self.handle, &mut handle);
        handle.expect("Expected JoinHandle, but got None").join().unwrap();
    }
}
