use std::process::Command;

fn main() {
    println!("cargo:rerun-if-changed=sass/main.scss");
    Command::new("sassc").arg("sass/main.scss")
                         .arg("static/css/main.css")
                         .status().unwrap();
}
