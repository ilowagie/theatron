#!/bin/sh
echo "making logging dirs:"
mkdir -pv var/log/{dev,prod}
echo "adding .secrets.json template"
echo -e "{\n\t\"api_key_map\": {\n\t\t\"imdb-api\": \"\"\n\t}\n}" > config/.secrets.json
