#!/bin/sh
# Oneliner to check if mp4 moov metadata is located before mdat
AtomicParsley $1 -T | head -2 | grep -q "moov"
# returns success code if the file has faststart
