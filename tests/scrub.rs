// TODO: update to newer version

// #![feature(proc_macro_hygiene, decl_macro)]

// #[macro_use] extern crate rocket;
// #[macro_use] extern crate rocket_contrib;

// use std::fs;
// use std::path::Path;

// use rocket::local::Client;
// use rocket::http::Status;
// use rocket::http::ContentType;

// use theatron;
// use theatron::scrubber;

// #[test]
// #[ignore]
// fn status_response() {
//     let rocket = theatron::rocket_init();
//     let client = Client::new(rocket).expect("valid rocket instance");
//     let response = client.post("/admin/db_mgmt/scrub_request")
//                          .header(ContentType::Form)
//                          .body("directory=tests/tests_workdir/orig/test-movie&movies=t")
//                          .dispatch();

//     assert_eq!(response.status(), Status::SeeOther);

//     let mut json_status = client.get("/admin/scrub_status_json").dispatch().body_string().unwrap();
//     let mut scrubber_status: scrubber::ScrubberStats = serde_json::from_str(json_status.as_str()).unwrap();
//     while scrubber_status.status == "working" {
//         let mut response = client.get("/admin/scrub_status_update").dispatch();
//         if response.status() == Status::Ok {
//             json_status = response.body_string().unwrap();
//             scrubber_status = serde_json::from_str(json_status.as_str()).unwrap();
//         }
//     }
//     println!("out of loop");

//     json_status = client.get("/admin/scrub_status_json").dispatch().body_string().unwrap();
//     scrubber_status = serde_json::from_str(json_status.as_str()).unwrap();
//     assert!(scrubber_status.status == "inactive" || scrubber_status.status == "done");
// }

// #[test]
// fn guess_title_test() {
//     let directory = String::from("tests/tests_workdir/orig/test-multiplemovies");
//     let mut rt = tokio::runtime::Runtime::new().unwrap();
//     rt.block_on(guess_title_test_helper(directory));
// }

// // NOTE: don't forget to ignore test or remove this part when everything works
// //       Kind of shady to share code with references to specific pirate torrents
// async fn guess_title_test_helper(directory: String) {
//     let path = Path::new(directory.as_str());
//     if let Ok(dir_entries) = fs::read_dir(path) {
//         for entry in dir_entries {
//             let file = entry.unwrap();
//             let path = file.path();
//             let path = path.as_path();
//             let name = String::from(path.file_name().unwrap().to_str().unwrap());
//             let (title, year) = scrubber::guess_title_year(&name).await.unwrap();
//             match name.as_str() {
//                 "Flash Gordon [1980] 1080p BluRay AAC x264-tomcat12[ETRG]" => {
//                     assert_eq!(title, String::from("Flash Gordon"));
//                     assert_eq!(year, String::from("1980"));
//                 },
//                 "Hearts.of.Darkness.1991.DVDRip.XviD-MakingOff.Org" => {
//                     assert_eq!(title, String::from("Hearts of Darkness"));
//                     assert_eq!(year, String::from("1991"));
//                 },
//                 "The.Bad.Lieutenant.Port.of.Call.New.Orleans.2009.1080p.BluRay.x264.anoXmous" => {
//                     assert_eq!(title, String::from("The Bad Lieutenant Port of Call New Orleans"));
//                     assert_eq!(year, String::from("2009"));
//                 },
//                 "Zardoz (1974) 1080p BrRip x264 - VPPV" => {
//                     assert_eq!(title, String::from("Zardoz"));
//                     assert_eq!(year, String::from("1974"));
//                 },
//                 _ => {
//                     println!("unexpected file discovered");
//                     assert!(false);
//                 }
//             };
//         }
//     } else {
//         println!("could not read directory");
//         assert!(false);
//     }
// }
