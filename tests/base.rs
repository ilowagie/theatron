#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;

use rocket::local::Client;
use rocket::http::Status;

use theatron;

#[test]
fn test_basics() {
    let rocket = theatron::rocket_init();
    let client = Client::new(rocket).expect("valid rocket instance");
    let response = client.get("/movies").dispatch();

    assert_eq!(response.status(), Status::Ok);
}

#[test]
fn test_redirect() {
    let rocket = theatron::rocket_init();
    let client = Client::new(rocket).expect("valid rocket instance");
    let response = client.get("/").dispatch();

    assert_eq!(response.status(), Status::SeeOther);
}
