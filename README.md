Theatron
========

Theatron is a web app that you can run locally or on your private server for ordering your movies and series.
It is made using rust and the rocket API.

How To Install
==============

At the moment, the only option is to clone the git repo and build the project from source.

Rocket requires you to use the rust-nightly toolchain.
To get this,
1. install `rustup`
2. run `rustup install nightly`

Once rust-nightly is installed, you can go ahead and make sure that Theatron's dependencies are installed.
Theatron requires the following dependencies:
1. a more or less POSIX compliant OS
2. `sassc` to compile the scss files to css
3. `wget` to download images from imdb when scrubbing directories
5. an IMDB API key, which you can register for for free
6. In some instances, the `ffprobe` command can be called. The command is normally part of `ffmpeg`
7. `ffmpeg` will also be used if you're trying to add movie containers or codecs that don't work well with streaming to a HTML5 player.
   If a movie file is (partially) not supported, then it is transcoded to `H.264` in `mp4`.
   In the configuration you can choose whether `ffmpeg` uses a hardware or software encoder.
   For the hardware encoder to work, you need the correct `VA-API` functionality.
8. the `file` command to detect a file's mime type
9. `AtomicParsley` is used to check if an mp4 file has faststart enabled (moov metadata before mdat)

Then, some directories and configuration are needed.
1. The default logging directories can be created manually or by running the `init_dirs.sh` script.
2. Your IMDB API key can be copied to the `config/.secrets.json` file under the `imdbToken` key.
   A default secrets file is created by the `init_dirs.sh` script.
4. before going on, it is also best to check the configuration in `config/theatron.json`

Once all this is done, launching theatron is as simple as running `cargo run`
Cargo will download the necessary crates and compile the project before launching it.
