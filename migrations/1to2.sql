insert into versions (id, project_version) values (2, "0.0.4");
create table if not exists episodes (
    episode_id      integer primary key,
    title           text,
    description     text,
    duration_mins   integer,
    imdb_id         text not null,
    episode_number  integer,
    added_at        text not null,
    is_complete     integer,
    is_present      integer,
    location        text
);
create table if not exists seasons (
    season_id               integer primary key,
    release_year            integer not null,
    season_number           integer,
    added_at                text not null,
    full_season_present     integer,
    full_season_complete    integer
);
create table if not exists series (
    series_id               integer primary key,
    title                   text not null,
    description             text,
    imdb_rating             text,
    release_start           u32,
    release_end             u32,
    poster_location         text,
    imdb_id                 text not null,
    avg_ep_duration_mins    u32,
    genres                  text,
    calc_avg_season         integer,
    added_at                text not null
);
create table if not exists series_cast_and_crew (
    scnc_id     integer primary key,
    crew_type   integer,
    rel_type    integer,
    role        text,
    product_id  integer,
    person_id   integer
);

create table if not exists series_seasons_rel (
    id          integer primary key,
    series_id   integer,
    season_id   integer,
    foreign key(series_id) references series(series_id),
    foreign key(season_id) references seasons(season_id)
);
create table if not exists seasons_episodes_rel (
    id          integer primary key,
    season_id   integer,
    episode_id  integer,
    foreign key(season_id) references seasons(season_id),
    foreign key(episode_id) references episodes(episode_id)
);
