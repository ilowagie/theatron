insert into versions (id, project_version) values ( 1, "0.0.3" );
create table movie_subs (
    sub_id          integer primary key,
    file_location   text not null,
    language        text not null,
    movie_id        integer,
    foreign key(movie_id) references movies(movie_id)
);
