insert into versions (id, project_version) values (3, "0.0.4");
drop table seasons;
create table seasons (
    season_id               integer primary key,
    release_year            integer not null,
    season_number           integer not null,
    series_imdb_id          text not null,
    added_at                text not null,
    full_season_present     integer,
    full_season_complete    integer
)
